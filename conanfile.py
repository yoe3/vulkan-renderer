"""
conanfile.py
"""
from conan import ConanFile


class YoeVulkanConan(ConanFile):
    """
    YoeVulkanConan
    """
    settings = "os", "arch", "compiler", "build_type"
    name = "Yoe-Vulkan"
    version = "0.1"
    generators = "CMakeDeps", "CMakeToolchain"

    def requirements(self):
        """
        requirements
        """
        self.requires("vulkan-headers/1.3.239.0", force=True)
        self.requires("vulkan-loader/1.3.239.0")
        self.requires("vulkan-validationlayers/1.3.239.0")
        self.requires("vulkan-memory-allocator/3.0.1")
        self.requires("sdl/2.28.5")
        # self.requires("shaderc/2021.1")
        self.requires("glslang/11.7.0")
        self.requires("spirv-tools/1.3.239.0", force=True)
        self.requires("spdlog/1.12.0")
        self.requires("stb/cci.20230920", force=True)
        self.requires("glm/cci.20230113")
        self.requires("assimp/5.2.2")
