param (
    [string]$ConanProfile = "default",
    [string]$BuildFolder = "build",
    [string]$CMakeGenerator = "Visual Studio 17 2022",
    # TODO: This parameter can only be Release or Debug
    [string]$BuildType = "Debug"
)

function Get-IsMultiGenerator() {
    if(${CMakeGenerator}.Contains("Visual Studio")) {
        return $True
    }

    return $False
}

function Install-Conan() {
    param (
        [string]$ConanBuildType
    )

    conan install . --output-folder=${BuildFolder} --profile ${ConanProfile} --build=missing --settings=build_type=${ConanBuildType} -c tools.cmake.cmaketoolchain:generator=${CMakeGenerator}
}

if(-not(Test-Path -Path ${BuildFolder})) {
    New-Item -Name ${BuildFolder} -ItemType Directory | Out-Null
}

# During conan install we can define what generator to use, like this:
# -c tools.cmake.cmaketoolchain:generator=Ninja
if(Get-IsMultiGenerator) {
    Install-Conan -ConanBuildType "Debug"
    Install-Conan -ConanBuildType "Release"
}
else {
    Install-Conan -ConanBuildType ${BuildType}
}

# TODO: How to figure out the right preset name - especially for multi generators. With multi generators the preset is called conan-default
# Write-Output "cmake --preset conan-$(${BuildType}.ToLower()) -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -S. -B${BuildFolder} -G ${CMakeGenerator}"
if(Get-IsMultiGenerator) {
    cmake --preset "conan-default" -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -S. "-B${BuildFolder}" -G ${CMakeGenerator}
}
else {
    cmake --preset "conan-$(${BuildType}.ToLower())" -DCMAKE_EXPORT_COMPILE_COMMANDS=1 -S. "-B${BuildFolder}" -G ${CMakeGenerator}
}
