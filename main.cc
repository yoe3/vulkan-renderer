#include <yoe/renderer.hh>
#include <yoe/window.hh>
#include <yoe/logger.hh>

#include <iostream>

int real_main()
{
    if(!yoe::logging::initialize(yoe::logging::log_level_type::debug))
    {
        std::cerr << "Failed to initialize logging system!" << std::endl;

        return EXIT_FAILURE;
    }
    yoe::window_type::initialiser_type window_initialiser;
    yoe::window_type window;
    yoe::renderer_type renderer("Yoe App", 0, window.get_hinstance(), window.get_hwnd());

    while(window.run())
    {
        renderer.run();
    }

    return EXIT_SUCCESS;
}

int main()
{
    try
    {
        return real_main();
    }
    catch(yoe::exception_type& exception)
    {
        yoe::logging::error("Exception cought from yoe: {}!", exception.what());

        return EXIT_FAILURE;
    }
}
