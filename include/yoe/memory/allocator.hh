#pragma once
#include <yoe/core/exception.hh>
#include <vk_mem_alloc.h>

namespace yoe
{
    class memory_allocator_type
    {
    public:
        class exception_type final: public yoe::exception_type
        {
        public:
            using yoe::exception_type::exception_type;
        };

        template<typename HandleT, void (*Destroy)(VmaAllocator, HandleT, VmaAllocation)>
        class handle_type
        {
        public:
            class mapped_memory_type
            {
            public:
                mapped_memory_type() = delete;
                mapped_memory_type(VmaAllocator allocator, VmaAllocation allocation):
                    _allocator(allocator),
                    _allocation(allocation)
                {
                    if(VK_SUCCESS != vmaMapMemory(_allocator, _allocation, &_pointer))
                    {
                        throw exception_type("Failed to map mempry!");
                    }
                }
                mapped_memory_type(const mapped_memory_type&) = delete;
                mapped_memory_type(mapped_memory_type&& mapped_memory) noexcept:
                    _allocator(mapped_memory._allocator),
                    _allocation(mapped_memory._allocation),
                    _pointer(mapped_memory._pointer)
                {
                    mapped_memory._allocator = VK_NULL_HANDLE;
                    mapped_memory._allocation = VK_NULL_HANDLE;
                    mapped_memory._pointer = nullptr;
                }

                ~mapped_memory_type()
                {
                    unmap();
                }

                mapped_memory_type& operator=(const mapped_memory_type&) = delete;
                mapped_memory_type& operator=(mapped_memory_type&& mapped_memory) noexcept
                {
                    if(this != &mapped_memory)
                    {
                        unmap();

                        _allocator = mapped_memory._allocator;
                        _allocation = mapped_memory._allocation;
                        _pointer = mapped_memory._pointer;

                        mapped_memory._allocator = VK_NULL_HANDLE;
                        mapped_memory._allocation = VK_NULL_HANDLE;
                        mapped_memory._pointer = nullptr;
                    }

                    return *this;
                }

                [[nodiscard]] void* get_pointer() const
                {
                    return _pointer;
                }

            private:
                void unmap()
                {
                    if(_pointer != nullptr)
                    {
                        vmaUnmapMemory(_allocator, _allocation);
                        _pointer = nullptr;
                    }
                }

                VmaAllocator _allocator = VK_NULL_HANDLE;
                VmaAllocation _allocation = VK_NULL_HANDLE;
                void* _pointer = nullptr;
            };

            handle_type() = default;
            handle_type(VmaAllocator allocator, VmaAllocation allocation, HandleT handle):
                _allocator(allocator),
                _allocation(allocation),
                _handle(handle)
            {
                // Nothing to do yet
            }
            handle_type(const handle_type&) = delete;
            handle_type(handle_type&& buffer) noexcept:
                _allocator(buffer._allocator),
                _allocation(buffer._allocation),
                _handle(buffer._handle)
            {
                buffer._allocator = VK_NULL_HANDLE;
                buffer._allocation = VK_NULL_HANDLE;
                buffer._handle = VK_NULL_HANDLE;
            }

            ~handle_type()
            {
                destroy();
            }

            [[nodiscard]] HandleT get_handle() const
            {
                return _handle;
            }

            mapped_memory_type map_memory() const
            {
                return mapped_memory_type(_allocator, _allocation);
            }

            handle_type& operator=(const handle_type&) = delete;
            handle_type& operator=(handle_type&& buffer) noexcept
            {
                if(this != &buffer)
                {
                    destroy();

                    _allocator = buffer._allocator;
                    _allocation = buffer._allocation;
                    _handle = buffer._handle;

                    buffer._allocator = VK_NULL_HANDLE;
                    buffer._allocation = VK_NULL_HANDLE;
                    buffer._handle = VK_NULL_HANDLE;
                }

                return *this;
            }

        private:
            void destroy()
            {
                if(_allocator != VK_NULL_HANDLE && _allocation != VK_NULL_HANDLE && _handle !=VK_NULL_HANDLE)
                {
                    Destroy(_allocator, _handle, _allocation);
                }
                _allocator = VK_NULL_HANDLE;
                _allocation = VK_NULL_HANDLE;
                _handle = VK_NULL_HANDLE;
            }

            VmaAllocator _allocator = VK_NULL_HANDLE;
            VmaAllocation _allocation = VK_NULL_HANDLE;
            HandleT _handle = VK_NULL_HANDLE;
        };

        using buffer_type = handle_type<VkBuffer, &vmaDestroyBuffer>;
        using image_type = handle_type<VkImage, &vmaDestroyImage>;

        memory_allocator_type() = default;
        ~memory_allocator_type();

        void initialize(VkInstance instance,
            VkPhysicalDevice physical_device,
            VkDevice device,
            uint32_t vulkanApiVersion);

        [[nodiscard]] buffer_type create_buffer(const VkBufferCreateInfo& buffer_create_info,
            bool mapped) const;
        [[nodiscard]] image_type create_image(const VkImageCreateInfo& image_create_info) const;

    private:
        VmaAllocator _allocator = VK_NULL_HANDLE;
    };
}
