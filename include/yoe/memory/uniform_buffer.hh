#pragma once
#include <glm/glm.hpp>

namespace yoe
{
    // See aligments: https://registry.khronos.org/vulkan/specs/1.3-extensions/html/chap15.html#interfaces-resources-layout
    template<typename T>
    struct align_as
    {
        // static_assert(false, "This struct needs to be specialised for each type!");
    };

    template<typename T>
    concept scalar = std::floating_point<T> || std::integral<T>;

    template<scalar T>
    struct align_as<T>
    {
        static constexpr size_t value = sizeof(T);
    };

    template<>
    struct align_as<float>
    {
        static constexpr size_t value = 4;
    };

    template<>
    struct align_as<glm::vec2>
    {
        static constexpr size_t value = 8;
    };

    template<>
    struct align_as<glm::vec3>
    {
        static constexpr size_t value = 16;
    };

    template<>
    struct align_as<glm::vec4>
    {
        static constexpr size_t value = 16;
    };

    // TODO: Check for structs somehow? (std::compound or similar)

    template<>
    struct align_as<glm::mat4>
    {
        static constexpr size_t value = 16;
    };

    struct frame_data_uniform_buffer_type
    {
        alignas(align_as<glm::vec3>::value) glm::vec3 cameraPosition;
        alignas(align_as<glm::vec3>::value) glm::vec3 cameraDirection;
    };

    struct object_data_uniform_buffer_type
    {
        alignas(align_as<glm::mat4>::value) glm::mat4 model = glm::mat4(1.0f);
        alignas(align_as<glm::mat4>::value) glm::mat4 view = glm::mat4(1.0f);
        alignas(align_as<glm::mat4>::value) glm::mat4 projection = glm::mat4(1.0f);
    };
}