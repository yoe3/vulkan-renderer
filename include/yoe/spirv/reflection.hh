#pragma once
#include <yoe/core/exception.hh>

#include <vector>
#include <spirv_reflect.h>

namespace yoe::spirv
{
    class reflection_type
    {
    public:
        class exception_type: public yoe::exception_type
        {
            using yoe::exception_type::exception_type;
        };

        using DescriptorSetLayoutBindingsType = std::vector<std::vector<vk::DescriptorSetLayoutBinding>>;

        explicit reflection_type(const std::vector<uint32_t>& code);
        [[nodiscard]] DescriptorSetLayoutBindingsType get_descriptor_set_layout_bindings() const;
        [[nodiscard]] std::vector<vk::VertexInputAttributeDescription> get_vertex_attribute_descriptions() const;

    private:
        [[nodiscard]] std::vector<SpvReflectInterfaceVariable*> get_input_variables() const;
        [[nodiscard]] std::vector<SpvReflectInterfaceVariable*> get_output_variables() const;
        [[nodiscard]] std::vector<SpvReflectBlockVariable*> get_push_constant_blocks() const;
        [[nodiscard]] std::vector<SpvReflectDescriptorBinding*> get_descriptor_bindings() const;
        [[nodiscard]] std::vector<SpvReflectDescriptorSet*> get_descriptor_sets() const;

        spv_reflect::ShaderModule _shader_module;
    };
}
