#pragma once
#include <yoe/core/exception.hh>

#include <vector>

namespace yoe
{
    class compiler_type
    {
    public:
        class exception_type final : public yoe::exception_type
        {
            using yoe::exception_type::exception_type;
        };

        class initialiser_type
        {
        public:
            initialiser_type();
            initialiser_type(const initialiser_type&) = delete;
            initialiser_type(initialiser_type&&) = delete;

            ~initialiser_type();

            initialiser_type& operator=(const initialiser_type&) = delete;
            initialiser_type& operator=(initialiser_type&&) = delete;
        };

        enum class shader_kind
        {
            VERTEX,
            FRAGMENT,
        };

        static std::vector<uint32_t> compile(shader_kind kind, const char* const* code, size_t count);

    private:

    };
}
