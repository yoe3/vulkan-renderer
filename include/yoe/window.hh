#pragma once
#include <yoe/core/exception.hh>

struct SDL_Window;

namespace yoe
{
    class window_type
    {
    public:
        class exception_type : public yoe::exception_type
        {
            using yoe::exception_type::exception_type;
        };

        class initialiser_type
        {
        public:
            initialiser_type();
            initialiser_type(const initialiser_type&) = delete;
            initialiser_type(initialiser_type&&) = delete;

            ~initialiser_type();

            initialiser_type& operator=(const initialiser_type&) = delete;
            initialiser_type& operator=(initialiser_type&&) = delete;
        };

        window_type();

        bool run();

        [[nodiscard]] HINSTANCE get_hinstance() const;
        [[nodiscard]] HWND get_hwnd() const;

    private:
        static void initialise();
        static void deinitialize();

        SDL_Window* _window = nullptr;
    };
}