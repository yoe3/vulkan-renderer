#pragma once
// TODO: vk::raii does not work with the following define
// #define VULKAN_HPP_NO_EXCEPTIONS
#include <vulkan/vulkan_raii.hpp>
#include <yoe/core/exception.hh>
#include <yoe/memory/allocator.hh>
#include <yoe/memory/uniform_buffer.hh>
#include <yoe/spirv/compiler.hh>
#include <vector>
#include <optional>
#include <map>
#include <chrono>

namespace yoe
{
    class image_type;
    class mesh_type;

    // TODO: Test the renderer with these assets: https://kenney.nl/assets/mini-dungeon
    class renderer_type
    {
    public:
        class exception_type : public yoe::exception_type
        {
        public:
            using yoe::exception_type::exception_type;
        };

        renderer_type(const char* application_name,
            uint32_t application_version,
            HINSTANCE instance,
            HWND hwnd);
        renderer_type(const renderer_type&) = delete;
        renderer_type(renderer_type&&) = delete;

        ~renderer_type();

        renderer_type& operator=(const renderer_type&) = delete;
        renderer_type& operator=(renderer_type&&) = delete;

        void run();

        class handle_type
        {
        public:
            handle_type() = default;
            explicit handle_type(size_t value):
                _value(value)
            {
                // Nothing to do yet
            }

            [[nodiscard]] bool is_valid() const
            {
                return _value == std::numeric_limits<size_t>::max();
            }

        private:
            friend renderer_type;
            explicit operator size_t() const
            {
                return _value;
            }

            size_t _value = std::numeric_limits<size_t>::max();
        };

        class image_handle_type: public handle_type
        {
        public:
            using handle_type::handle_type;
        };
        image_handle_type create_image(const image_type& image);

        class shader_handle_type: public handle_type
        {
        public:
            using handle_type::handle_type;
        };
        shader_handle_type create_shader(const char* vertex_code, const char* fragment_code);

        class material_handle_type: public handle_type
        {
        public:
            using handle_type::handle_type;
        };
        material_handle_type create_material(shader_handle_type shader,
            image_handle_type albedo);

        class mesh_handle_type: public handle_type
        {
        public:
            using handle_type::handle_type;
        };
        mesh_handle_type create_mesh(const mesh_type& mesh,
            material_handle_type material,
            const glm::vec3& position);

    private:
        class pipeline_handle_type: public handle_type
        {
        public:
            using handle_type::handle_type;
        };
        pipeline_handle_type get_pipeline(mesh_handle_type mesh_handle);
        pipeline_handle_type create_pipeline(mesh_handle_type mesh_handle);

        struct PhysicalDevice
        {
            vk::raii::PhysicalDevice physical_device = nullptr;
            uint32_t graphics_queue_family_index = 0;
            uint32_t compute_queue_family_index = 0;
            uint32_t transfer_queue_family_index = 0;
            uint32_t presentation_queue_family_index = 0;
        };

        template<typename VkEnumT, typename VkHppEnumT>
        VkEnumT convert_to_vk_enum(VkHppEnumT vkHppEnum)
        {
            const auto value = static_cast<std::underlying_type_t<VkHppEnumT>>(vkHppEnum);

            return static_cast<VkEnumT>(value);
        }

        template<typename VkHppEnumtT, typename VkEnumT>
        VkHppEnumtT convert_to_vk_hpp_enum(VkEnumT vkEnum)
        {
            const auto value = static_cast<std::underlying_type_t<VkEnumT>>(vkEnum);

            return static_cast<VkHppEnumtT>(value);
        }

        template<typename ToEnumT, typename FromEnumT>
        ToEnumT convert_enum(FromEnumT from_enum)
        {
            const auto value = static_cast<std::underlying_type_t<FromEnumT>>(from_enum);

            return static_cast<ToEnumT>(value);
        }

        template<typename HandleT, typename CreateInfoT>
        class HandleInfoHolder
        {
        public:
            HandleInfoHolder() = default;
            HandleInfoHolder(HandleT handle, const CreateInfoT& create_info):
                _handle(std::move(handle)),
                _create_info(create_info)
            {
                // Nothing to do yet
            }

            [[nodiscard]] const HandleT& get_handle() const
            {
                return _handle;
            }

            [[nodiscard]] const CreateInfoT& get_create_info() const
            {
                return _create_info;
            }

        private:
            HandleT _handle = nullptr;
            CreateInfoT _create_info = {};
        };

        using SwapchainT = HandleInfoHolder<vk::raii::SwapchainKHR, VkSwapchainCreateInfoKHR>;

        // TODO: Make all of these standalone functions
        void check_required_instance_extensions_support(const std::vector<const char*>& required_extension_names) const;
        void check_required_instance_layers_support(const std::vector<const char*>& required_layer_names) const;
        [[nodiscard]] PhysicalDevice select_physical_device(const std::vector<const char*>& required_extension_names,
            const std::vector<const char*>& required_layer_names) const;
        [[nodiscard]] vk::raii::Device create_device(const std::vector<const char*>& required_extensions,
            const std::vector<const char*>& required_layers) const;
        [[nodiscard]] SwapchainT create_swapchain() const;
        [[nodiscard]] vk::raii::ShaderModule create_shader_module(compiler_type::shader_kind kind, const std::vector<uint32_t>& spirv_code) const;
        [[nodiscard]] memory_allocator_type::buffer_type create_buffer(VkDeviceSize size, VkBufferUsageFlags usage, bool map,const void* data = nullptr) const;
        [[nodiscard]] image_handle_type create_image(const char* path);

        compiler_type::initialiser_type _spirv_compiler_initialiser;
        vk::raii::Context _context;
        vk::raii::Instance _instance = nullptr;
#ifdef YOE_DEBUG
        vk::raii::DebugUtilsMessengerEXT _debug_utils_messenger = nullptr;
#endif
        vk::raii::SurfaceKHR _surface = nullptr;
        PhysicalDevice _physical_device;
        vk::raii::Device _device = nullptr;
        vk::raii::Queue _graphics_queue = nullptr;
        vk::raii::Queue _compute_queue = nullptr;
        vk::raii::Queue _transfer_queue = nullptr;
        vk::raii::Queue _presentation_queue = nullptr;
        SwapchainT _swapchain;
        std::vector<vk::raii::ImageView> _swapchain_image_views;
        memory_allocator_type _memory_allocator;
        // One depth image is enough, since we do not use it from the CPU
        // it is only used by the GPU while commands are executing.
        // And onyl one pipeline can be executed at the same time
        memory_allocator_type::image_type _depth_image;
        vk::raii::ImageView _depth_image_view = nullptr;
        vk::raii::RenderPass _render_pass = nullptr;
        std::vector<vk::raii::Framebuffer> _framebuffers;
        std::vector<vk::raii::Semaphore> _swapcain_semaphores;
        std::vector<vk::raii::Semaphore> _rendering_semaphores;
        vk::raii::CommandPool _graphics_command_pool = nullptr;
        vk::raii::CommandPool _transfer_command_pool = nullptr;
        std::vector<vk::raii::CommandBuffer> _graphics_command_buffers;
        vk::raii::CommandBuffer _transfer_command_buffer = nullptr;
        std::vector<vk::raii::Fence> _graphics_command_buffer_fences;
        vk::raii::DescriptorSetLayout _descriptor_set_layout0 = nullptr;
        vk::raii::DescriptorSetLayout _descriptor_set_layout1 = nullptr;
        vk::raii::DescriptorPool _descriptor_pool = nullptr;
        std::vector<vk::raii::DescriptorSet> _descriptor_sets0;
        std::vector<vk::raii::DescriptorSet> _descriptor_sets1;
        vk::raii::PipelineLayout _graphics_pipeline_layout = nullptr;
        vk::raii::Pipeline _graphics_pipeline = nullptr;
        memory_allocator_type::buffer_type _vertex_buffer; // TODO: Remove this
        // We need as many uniform data as many frames can be used (depending on the swapchain)
        // since we are going to update them from the CPU and we do not want to update the uniform buffer
        // of frame n, while we are preparing the data for frame n+1
        std::vector<memory_allocator_type::buffer_type> _uniform_buffers;
        std::vector<memory_allocator_type::buffer_type::mapped_memory_type> _uniform_buffer_mappings;
        object_data_uniform_buffer_type _uniform_buffer_data0;
        object_data_uniform_buffer_type _uniform_buffer_data1;
        struct image_data_type
        {
            memory_allocator_type::image_type image;
            vk::raii::ImageView view = nullptr;
            vk::raii::Sampler sampler = nullptr;
        };
        std::vector<image_data_type> _images;

        struct mesh_data_type
        {
            memory_allocator_type::buffer_type _vertex_buffer;
            std::vector<vk::VertexInputAttributeDescription> _vertex_input_attribute_descriptions;
            memory_allocator_type::buffer_type _index_buffer;
            std::vector<memory_allocator_type::buffer_type> _object_data_uniform_buffers;
            object_data_uniform_buffer_type _object_data_uniform_buffer;
            std::vector<memory_allocator_type::buffer_type> _frame_data_uniform_buffers;
            frame_data_uniform_buffer_type _frame_data_uniform_buffer;
            uint32_t _index_count;
            uint32_t _stride_size;
            vk::raii::DescriptorPool _descriptor_pool;
            std::vector<vk::raii::DescriptorSet> _descriptor_sets;
            material_handle_type _material{};
            pipeline_handle_type _pipeline;
        };
        std::vector<mesh_data_type> _meshes;

        size_t get_pipeline_hash(mesh_handle_type mesh_handle) const;
        std::map<size_t, pipeline_handle_type> _pipeline_map;

        struct shader_data_type
        {
            vk::raii::ShaderModule _vertex_shader;
            vk::raii::ShaderModule _fragment_shader;
            // TODO: This is the same type as in spirv::reflection_type
            std::vector<std::vector<vk::DescriptorSetLayoutBinding>> _descriptor_set_layout_bindings;
            std::vector<vk::raii::DescriptorSetLayout> _descriptor_set_layouts;
            std::vector<vk::VertexInputAttributeDescription> _vertex_input_attribute_descriptions;
            vk::raii::PipelineLayout _pipeline_layout;
        };
        std::vector<shader_data_type> _shaders;

        struct material_data_type
        {
            shader_handle_type _shader = {};
            image_handle_type _albedo_texture = {};
        };
        std::vector<material_data_type> _materials;

        std::vector<vk::raii::Pipeline> _pipelines;

        uint64_t _current_frame = 0;
        uint64_t _frame_index = 0;
        using seconds_type = std::chrono::duration<float>;
        using clock_type = std::chrono::high_resolution_clock;
        using time_point_type = clock_type::time_point;
        seconds_type _delta_time{};
        time_point_type _previous_time = clock_type::now();
    };
}
