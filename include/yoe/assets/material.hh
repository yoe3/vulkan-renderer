#pragma once
#include <yoe/renderer.hh>

#include <vector>

namespace yoe
{
    class material_type
    {
    public:
        material_type();
        explicit material_type(std::vector<renderer_type::image_handle_type> images);

        void add_image(renderer_type::image_handle_type image);

    private:
        std::vector<renderer_type::image_handle_type> _images;
    };
}