#pragma once
#include <yoe/core/exception.hh>

#include "material.hh"

class aiMesh;

namespace yoe
{
    class mesh_type
    {
    public:
        class exception_type: public yoe::exception_type
        {
        public:
            using yoe::exception_type::exception_type;
        };

        explicit mesh_type(const char* path);

        [[nodiscard]] const std::vector<uint8_t>& get_vertex_data() const
        {
            return _mesh_data.vertex_data;
        }

        [[nodiscard]] const std::vector<uint16_t>& get_index_data() const
        {
            return _mesh_data.index_data;
        }

        [[nodiscard]] std::vector<vk::VertexInputAttributeDescription> get_vertex_attribute_descriptions() const;

        [[nodiscard]] size_t get_stride_size() const;

    private:
        enum class vertex_format_type: uint8_t
        {
            position,
            normal,
            texture_ccordinates1,
            texture_ccordinates2,
            vertex_colour,
            tangent,
            bitangent
        };

        class vertex_format_descriptor_type
        {
        public:
            vertex_format_descriptor_type() = default;
            explicit vertex_format_descriptor_type(std::vector<vertex_format_type> vertex_format);

            [[nodiscard]] static constexpr size_t get_component_size(vertex_format_type vertex_format);

            [[nodiscard]] size_t get_stride_size() const;
            [[nodiscard]] uint32_t get_offset(vertex_format_type vertex_format) const;
            [[nodiscard]] const std::vector<vertex_format_type>& get_vertex_format() const;

            [[nodiscard]] bool operator==(const vertex_format_descriptor_type&) const = default;

        private:
            std::vector<vertex_format_type> _vertex_format;
        };

        static std::vector<vertex_format_type> get_vertex_format(const aiMesh* mesh);
        static std::vector<uint8_t> get_vertex_data(const aiMesh* mesh, const vertex_format_descriptor_type& descriptor);
        static size_t get_vertex_data_size(const aiMesh* mesh, const vertex_format_descriptor_type& descriptor);
        static vk::Format convert(vertex_format_type vertex_format);
        static uint32_t get_location(vertex_format_type vertex_format);

        struct mesh_data_type
        {
            std::vector<uint8_t> vertex_data;
            std::vector<uint16_t> index_data;
            vertex_format_descriptor_type vertex_format_descriptor;
        };
        struct submes_datum_type
        {
            uint16_t vertex_data_offset;
            uint16_t vertex_data_length;
            uint16_t index_data_offset;
            uint16_t index_data_length;
            uint8_t material_index;
        };

        mesh_data_type _mesh_data;
        std::vector<submes_datum_type> _submesh_data;
        std::vector<material_type> _materials;
    };

    constexpr size_t mesh_type::vertex_format_descriptor_type::get_component_size(
        vertex_format_type vertex_format)
    {
        switch(vertex_format)
        {
            case vertex_format_type::position:
                return 3 * sizeof(float);
            case vertex_format_type::normal:
                return 3 * sizeof(float);
            case vertex_format_type::texture_ccordinates1:
                return 2 * sizeof(float);
            case vertex_format_type::texture_ccordinates2:
                return 2 * sizeof(float);
            case vertex_format_type::vertex_colour:
                return 4 * sizeof(float);
            case vertex_format_type::tangent:
                return 3 * sizeof(float);
            case vertex_format_type::bitangent:
                return 3 * sizeof(float);
        }

        throw exception_type("Unknonw vertex format recevied: {}!", static_cast<std::underlying_type_t<vertex_format_type>>(vertex_format));

        return 0;
    }
}
