#pragma once
#include <yoe/core/exception.hh>

namespace yoe
{
    class image_type
    {
    public:
        class exception_type final: public yoe::exception_type
        {
        public:
            using yoe::exception_type::exception_type;
        };

        enum class format_type: uint8_t
        {
            greyscale,
            rgb,
            rgba,
        };

        explicit image_type(const char* path);
        image_type(const image_type&) = delete;
        image_type(image_type&& image) = delete;

        ~image_type();

        image_type& operator=(const image_type&) = delete;
        image_type& operator=(image_type&&) = delete;

        [[nodiscard]] unsigned char* get_data() const;
        [[nodiscard]] uint32_t get_width() const;
        [[nodiscard]] uint32_t get_height() const;
        [[nodiscard]] uint32_t get_components() const;
        [[nodiscard]] format_type get_format() const;
        [[nodiscard]] size_t get_size() const;

    private:
        unsigned char* _data = nullptr;
        uint32_t _width = 0;
        uint32_t _height = 0;
        uint32_t _components = 0;
    };
} // namespace yoe
