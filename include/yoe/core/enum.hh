#pragma once
#include <type_traits>

namespace yoe
{
    template<typename type>
    concept enumeration = std::is_enum_v<type>;

    template<enumeration enum_type>
    constexpr std::underlying_type_t<enum_type> enum_to_underlying_type(enum_type value)
    {
        return static_cast<std::underlying_type_t<enum_type>>(value);
    }

    template<enumeration enum_type>
    const char* enum_to_string(enum_type value)
    {
        static_assert(!std::is_enum_v<enum_type>, "This method needs to be specialised for every enum type!");

        // Just to make the IDE happy
        return nullptr;
    }

}
