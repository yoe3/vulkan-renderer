#pragma once
#include <exception>
#include <string>
#include <format>

namespace yoe
{
    class exception_type: public std::exception
    {
    public:
        // TODO: Move implementation to source file
        explicit exception_type(std::string message):
            _message(std::move(message))
        {
            // Nothing to do yet
        }

        template<typename... ArgumentTypes>
        explicit exception_type(const char* format, ArgumentTypes&&... arguments):
            _message(std::vformat(format, std::make_format_args(arguments...)))
        {
            // Nothing to do yet
        }

        [[nodiscard]] const char* what() const noexcept override
        {
            return _message.c_str();
        }

    private:
        std::string _message;
    };
}
