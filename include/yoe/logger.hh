#pragma once
#include <fmt/format.h>
#include <spdlog/spdlog.h>

#include <yoe/core/exception.hh>

namespace yoe::logging
{
    enum class log_level_type: uint8_t
    {
        debug,
        info,
        warning,
        error,
        critical
    };

    // For format options, see: https://github.com/gabime/spdlog/wiki/3.-Custom-formatting
    [[nodiscard]] bool initialize(log_level_type log_level,
        const char* path = nullptr,
        const char* format = nullptr);

    template<typename... argument_types>
    void debug(fmt::format_string<argument_types...> format, argument_types&&... arguments);

    template<typename... argument_types>
    void info(const char* format, argument_types&&... arguments);

    template<typename... argument_types>
    void warning(fmt::format_string<argument_types...> format, argument_types&&... arguments);

    template<typename... argument_types>
    void error(fmt::format_string<argument_types...> format, argument_types&&... arguments);

    template<typename... argument_types>
    void critical(const char* format, argument_types&&... arguments);

    class logger_type
    {
    public:
        class exception_type: public yoe::exception_type
        {
        public:
            using yoe::exception_type::exception_type;
        };

        logger_type(const char* name, log_level_type log_level, const char* format = nullptr);

        template<typename... argument_types>
        void debug(fmt::format_string<argument_types...> format, argument_types&&... arguments);

        template<typename... argument_types>
        void info(fmt::format_string<argument_types...> format, argument_types&&... arguments);

        template<typename... argument_types>
        void warning(fmt::format_string<argument_types...> format, argument_types&&... arguments);

        template<typename... argument_types>
        void error(fmt::format_string<argument_types...> format, argument_types&&... arguments);

        template<typename... argument_types>
        void critical(fmt::format_string<argument_types...> format, argument_types&&... arguments);

    private:
        std::shared_ptr<spdlog::logger> _logger;
    };
} // namespace yoe::logging

namespace yoe::logging
{
    //
    // "global" logging methods
    //

    template<typename... argument_types>
    inline void debug(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        spdlog::debug(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void info(const char* format, argument_types&&... arguments)
    {
        spdlog::info(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void warning(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        spdlog::warn(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void error(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        spdlog::error(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void critical(const char* format, argument_types&&... arguments)
    {
        spdlog::critical(format, std::forward<argument_types>(arguments)...);
    }

    //
    // "per logger" logging methods
    //

    template<typename... argument_types>
    inline void logger_type::debug(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        _logger->debug(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void logger_type::info(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        _logger->info(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void logger_type::warning(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        _logger->warn(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void logger_type::error(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        _logger->error(format, std::forward<argument_types>(arguments)...);
    }

    template<typename... argument_types>
    inline void logger_type::critical(fmt::format_string<argument_types...> format, argument_types&&... arguments)
    {
        _logger->critical(format, std::forward<argument_types>(arguments)...);
    }
} // namespace yoe::logging
