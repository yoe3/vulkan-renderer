# Pipeline inputs:
- Shaders
- Vertex format
- (Polygon mode (fill, line), cull mode (front, back))
- Depth/Stencil read/write
- Blending

# How does mesh rendering work?
For a mesh to be rendered we need vertex/index data, a shader program and a way to provide the needed inputs for the shader program.

## Creating shaders

So when a shader program is created in `renderer_type::create_shader` the required input fields
are read from the shader program using `SPIR-V Cross`.

There are two kinds of input data a shader can have:

### Vertex input
Vertex shaders expect a certain set of vertex input data (e.g. position, normals and so on)

This information can be fetched using `SPIR-v Cross`, and converted into a list of `VkVertexInputAttributeDescription`.

This list of vertex input description includes the location and format of each vertex attributes the shader expects.

In the shaders, certain vertex attributes are hardcoded to certain locations. E.g. the `position` is always at location `0`

### Uniforms and textures
The other input a shader program can have are uniforms and textures.

In `Vulkan`, these inputs are bound through descriptor sets, so we need to fetch the expected descriptor set from the shader.

This can also be done by `SPRI-V Cross`.

We get a list of sets, and each set contains a list of `VkDescriptorSetLayoutBinding`.
In those `VkDescriptorSetLayoutBinding`s we store the binding points, the type, the count and the stage flags (e.g. vertex or fragment).

A shader program consists of several stages, and each stage can have its on input uniforms/textures,
but it might be that they refer to the same binding in more than one stage.

Because of that, after fetching the descriptor set layout bindings, we need to "merge" them into one,
by simply eliminating any duplications. Only descriptors in the same set need to be merged.

All of this is basically needed in order to be able to create a list of `VkDescriptorSetLayout`s and a `VkPipelineLayout`
which contains everything the shader requires.

Again, in the shaders, certain uniforms and textures are bound to hardcoded sets and binding points.

E.g.: set `0` is for uniform buffers, set `1` is for textures. Within set `0` the first binding point is for frame data, 
while the second one is for object data.

## Creating textures
Creating textures is fairly simple, no real additional data is generated.

Together with the actual `VkImage` a `VkImageView` and a `VkSampler` are also created, since they are essential for the usage.

## Creating materials
Material is not a concept in `Vulkan`, and they are basically just a compound resource, created from a shader and a texture.

Currently only a single albedo texture is supported for the materials.

## Creating meshes
Creating a mesh is a complex process. The logic is handled in `renderer_type::create_mesh` When it is created it is bound to a material.

First the buffers for vertex and index data are created.

### Creating and updating descriptor sets
Then a descriptor pool is created, based on the given shader from the material.
A descriptor pool needs to know how many of each descriptor type it needs to allocate maximum.
For that we iterate over all the descriptor types required for the shader and simply count them by their type.

Then this descriptor pool can be used to actually allocate the descriptor sets, as many as the shader requires.

When we have the descriptor sets, we need to update them so they "point" to some valid `Vulkan` object.
For textures, it needs to be a `VkImageView` and a `VkSampler` for uniforms it needs to be a `VkBuffer`.

One tricky thing is that for uniform buffers, we need to allocate as many uniform buffers and desciptor sets, as many swapchain images we have.

This means we allocate more descriptor sets than the shader requires, but most of them will be the same, they will just be used for different frames.

This is the main reason why uniforms and textures are on different sets. We only need to do this additional allocation for uniform buffers.
(Since textures are read only, we do not need to think about CPU-GPU synchronisation)

So, to update the descriptor sets, we create the uniforms buffers and "bind" them to the descriptor set.
They are also initialised at the same time.

For textures, we "bind" both the texture view and the sampler.

### Finding an existing pipeline for the mesh
When the descriptor sets are updated, the next step is to try to find an existing pipeline for the mesh or create a new one.

To find a compatible pipeline for the mesh, a few things need to be considered:

- The stride size of the mesh's vertex data
- The shader to be used
- And the pipeline layout from the shader

These values are combined into a hash, and this hash is used to find a compatible pipeline for the mesh.

If there is no such pipeline a new one needs to be created!

### Creating a new pipeline for the mesh
Creating a pipeline contains a lot of steps for each step. Many of these steps (for now) are fixed,
and does not matter what mesh is being used with it.

The important parts are the following:

#### Creating `VkPipelineVertexInputStateCreateInfo`
This struct defines the vertex input handling. Where the different vertex attributes are coming from, which buffer,
and where they are located within that buffer.

Both the mesh and the shader have a list of `VkVertexInputAttributeDescription`. For the mesh it describes the vertex attributes it can provide,
for the shader it describes the vertex attributes it requires. We need to match the two.

There can be three scenarios:

- The vertex attribute **IS** provided by the mesh, but it is **NOT** required by the shader
    - There is nothing to do in this case we can simply ignore this attribute
- The vertex attribute is **NOT** provided by the mesh, but it **IS** required by the shader
    - We have a problem here. Currently, the code throws an exception.
      It should be possible to provide a reasonable default value with a buffer.
      The problem is that we do not know how many vertices is going to be rendered, so we do not know how big the buffer should be.
      In `Metal` you cna play around with the input rate of the vertex, and set it to `Constant` meaning the same and only value will be used for all the vertices
      But in `Vulkan` we do not have such an option, the input rate can only be `Vertex` or `Instance`.
- The vertex attribute **IS** provided by the mesh, and it **IS** required by the shader
    - All is good, we can add this vertex attribute to the list of `VkVertexInputAttributeDescription` with the proper offset
 
#### Creating the pipeline
The rest of the pipeline is fixed (for now) so after the vertex input is set up correctly in can finally be created.

For the pipeline layout we can use the one created for the shader.

## Rendering the meshes
After all this setup, rendering a mesh is really simple. (That was the goal. Rendering needs to be fast!)

The following steps needs to be done:

- Bind the pipeline used for the mesh
- Bind the right desciptor sets
    - For uniform buffers we need to select the one for the actual frame
    - For textures, we can just use the created ones
- Bind the vertex and the index buffers of the mesh
- Use `drawIndexed` to draw the mesh

That should be it. With this setup meshes can dynamically be drawn in the render loop. All the required data and setup is done at creation time.

It is still fairly limited. Only one texture is supported, the pipeline is mostly fixed!
Also, different renderpasses might require different pipelines if they are not compatible. So there is still a lot to do.