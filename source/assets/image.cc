#include <yoe/assets/image.hh>
#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

yoe::image_type::image_type(const char* path)
{
    int width = 0;
    int height = 0;
    int components = 0;

    _data = stbi_load(path, &width, &height, &components, 0);
    if(_data == nullptr)
    {
        throw exception_type("Failed to load image at {}", path);
    }
    if(components == 2)
    {
        throw exception_type("Transparent greyscale images are not supported!");
    }

    _width = static_cast<uint32_t>(width);
    _height = static_cast<uint32_t>(height);
    _components = static_cast<uint32_t>(components);
}

yoe::image_type::~image_type()
{
    stbi_image_free(_data);
}

unsigned char* yoe::image_type::get_data() const
{
    return _data;
}

uint32_t yoe::image_type::get_width() const
{
    return _width;
}

uint32_t yoe::image_type::get_height() const
{
    return _height;
}

uint32_t yoe::image_type::get_components() const
{
    return _components;
}

yoe::image_type::format_type yoe::image_type::get_format() const
{
    if(get_components() == 1)
    {
        return format_type::greyscale;
    }
    if(get_components() == 3)
    {
        return format_type::rgb;
    }
    if(get_components() == 4)
    {
        return format_type::rgba;
    }

    throw exception_type("Invalid number of components found: {}", get_components());
}

size_t yoe::image_type::get_size() const
{
    return static_cast<size_t>(_width) * _height * _components;
}
