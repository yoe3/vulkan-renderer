#include <yoe/assets/mesh.hh>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/mesh.h>

namespace
{
    std::vector<uint16_t> get_index_data(const aiMesh* mesh)
    {
        std::vector<uint16_t> result;
        result.reserve(mesh->mNumFaces * 3);

        for(unsigned int index = 0; index < mesh->mNumFaces; ++index)
        {
            const aiFace& face = mesh->mFaces[index];

            for(unsigned int face_index = 0; face_index < face.mNumIndices; ++face_index)
            {
                if(face.mIndices[face_index] > std::numeric_limits<uint16_t>::max())
                {
                    throw yoe::mesh_type::exception_type("Index value ({}) is bigger than max supported ({})!",
                        face.mIndices[face_index],
                        face.mIndices[face_index]);
                }
                const uint16_t value = face.mIndices[face_index];
                result.push_back(value);
            }
        }

        return result;
    }

    size_t get_index_data_size(const aiMesh* mesh)
    {
        return mesh->mNumFaces * 3;
    }
}

yoe::mesh_type::vertex_format_descriptor_type::vertex_format_descriptor_type(std::vector<vertex_format_type> vertex_format):
    _vertex_format(std::move(vertex_format))
{
    // Nothing to do yet
}

size_t yoe::mesh_type::vertex_format_descriptor_type::get_stride_size() const
{
    size_t result = 0;

    for(const auto vertex_format: _vertex_format)
    {
        switch(vertex_format)
        {
            case vertex_format_type::position:
                result += get_component_size(vertex_format_type::position);
                break;
            case vertex_format_type::normal:
                result += get_component_size(vertex_format_type::normal);
                break;
            case vertex_format_type::texture_ccordinates1:
                result += get_component_size(vertex_format_type::texture_ccordinates1);
                break;
            case vertex_format_type::texture_ccordinates2:
                result += get_component_size(vertex_format_type::texture_ccordinates2);
                break;
            case vertex_format_type::vertex_colour:
                result += get_component_size(vertex_format_type::vertex_colour);
                break;
            case vertex_format_type::tangent:
                result += get_component_size(vertex_format_type::tangent);
                break;
            case vertex_format_type::bitangent:
                result += get_component_size(vertex_format_type::bitangent);
                break;
        }
    }

    return result;
}

uint32_t yoe::mesh_type::vertex_format_descriptor_type::get_offset(vertex_format_type vertex_format) const
{
    const auto existing = std::find(_vertex_format.cbegin(), _vertex_format.cend(), vertex_format);

    if(existing == _vertex_format.cend())
    {
        throw exception_type("Offset of given vertex format ({}), cannot be calculated, since the mesh does not provide it!",
            static_cast<std::underlying_type_t<vertex_format_type>>(vertex_format));
    }

    uint32_t offset = 0;

    for(auto iterator = _vertex_format.cbegin(); iterator != existing; ++iterator)
    {
        offset += get_component_size(*iterator);
    }

    return offset;
}

const std::vector<yoe::mesh_type::vertex_format_type>& yoe::mesh_type::vertex_format_descriptor_type::get_vertex_format() const
{
    return _vertex_format;
}

std::vector<yoe::mesh_type::vertex_format_type> yoe::mesh_type::get_vertex_format(const aiMesh* mesh)
{
    std::vector<vertex_format_type> result;

    if(mesh->HasPositions())
    {
        result.push_back(vertex_format_type::position);
    }
    if(mesh->HasNormals())
    {
        result.push_back(vertex_format_type::normal);
    }
    if(mesh->HasTextureCoords(0))
    {
        result.push_back(vertex_format_type::texture_ccordinates1);
    }
    if(mesh->HasTextureCoords(1))
    {
        result.push_back(vertex_format_type::texture_ccordinates2);
    }
    if(mesh->HasVertexColors(0))
    {
        result.push_back(vertex_format_type::vertex_colour);
    }
    if(mesh->HasTangentsAndBitangents())
    {
        result.push_back(vertex_format_type::tangent);
        result.push_back(vertex_format_type::bitangent);
    }

    if(result.empty())
    {
        throw exception_type("Mesh has no supported vertex format!");
    }

    return result;
}

std::vector<uint8_t> yoe::mesh_type::get_vertex_data(const aiMesh* mesh, const vertex_format_descriptor_type& descriptor)
{
    std::vector<uint8_t> result;
    result.resize(descriptor.get_stride_size() * mesh->mNumVertices);
    uint8_t* data = result.data();

    for(int index = 0; index < mesh->mNumVertices; ++index)
    {
        if(mesh->HasPositions())
        {
            memcpy(data, &mesh->mVertices[index], vertex_format_descriptor_type::get_component_size(vertex_format_type::position));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::position);
        }
        if(mesh->HasNormals())
        {
            memcpy(data, &mesh->mNormals[index], vertex_format_descriptor_type::get_component_size(vertex_format_type::normal));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::normal);
        }
        if(mesh->HasTextureCoords(0))
        {
            memcpy(data, &mesh->mTextureCoords[0][index], vertex_format_descriptor_type::get_component_size(vertex_format_type::texture_ccordinates1));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::texture_ccordinates1);
        }
        if(mesh->HasTextureCoords(1))
        {
            memcpy(data, &mesh->mTextureCoords[1][index], vertex_format_descriptor_type::get_component_size(vertex_format_type::texture_ccordinates2));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::texture_ccordinates2);
        }
        if(mesh->HasVertexColors(0))
        {
            memcpy(data, &mesh->mColors[0][index], vertex_format_descriptor_type::get_component_size(vertex_format_type::vertex_colour));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::vertex_colour);
        }
        if(mesh->HasTangentsAndBitangents())
        {
            memcpy(data, &mesh->mTangents[index], vertex_format_descriptor_type::get_component_size(vertex_format_type::tangent));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::tangent);
            memcpy(data, &mesh->mBitangents[index], vertex_format_descriptor_type::get_component_size(vertex_format_type::bitangent));
            data += vertex_format_descriptor_type::get_component_size(vertex_format_type::bitangent);
        }
    }

    return result;
}

size_t yoe::mesh_type::get_vertex_data_size(const aiMesh* mesh, const vertex_format_descriptor_type& descriptor)
{
    const size_t stride_size = descriptor.get_stride_size();

    return stride_size * mesh->mNumVertices;
}

vk::Format yoe::mesh_type::convert(vertex_format_type vertex_format)
{
    switch(vertex_format)
    {
        case vertex_format_type::position:
            return vk::Format::eR32G32B32Sfloat;
        case vertex_format_type::normal:
            return vk::Format::eR32G32B32Sfloat;
        case vertex_format_type::texture_ccordinates1:
            return vk::Format::eR32G32Sfloat;
        case vertex_format_type::texture_ccordinates2:
            return vk::Format::eR32G32Sfloat;
        case vertex_format_type::vertex_colour:
            return vk::Format::eR32G32B32A32Sfloat;
        case vertex_format_type::tangent:
            return vk::Format::eR32G32B32Sfloat;
        case vertex_format_type::bitangent:
            return vk::Format::eR32G32B32Sfloat;
    }

    throw exception_type("Unknown vertexformat received: {}",
        static_cast<std::underlying_type_t<vertex_format_type>>(vertex_format));
}

uint32_t yoe::mesh_type::get_location(vertex_format_type vertex_format)
{
    switch(vertex_format)
    {
        case vertex_format_type::position:
            return 0;
        case vertex_format_type::normal:
            return 1;
        case vertex_format_type::texture_ccordinates1:
            return 2;
        case vertex_format_type::texture_ccordinates2:
            return 3;
        case vertex_format_type::vertex_colour:
            return 4;
        case vertex_format_type::tangent:
            return 5;
        case vertex_format_type::bitangent:
            return 6;
    }

    throw exception_type("Unknown vertexformat received: {}",
        static_cast<std::underlying_type_t<vertex_format_type>>(vertex_format));
}

yoe::mesh_type::mesh_type(const char* path)
{
    Assimp::Importer importer;
    const aiScene* scene = importer.ReadFile(path, aiProcess_CalcTangentSpace | aiProcess_Triangulate | aiProcess_ImproveCacheLocality);

    if(scene == nullptr)
    {
        throw exception_type("Failed to import file at {}! Error message: {}", path, importer.GetErrorString());
    }
    if(0 == scene->mNumMeshes)
    {
        throw exception_type("No meshes found in file at {}!", path);
    }

    // Here we know there is at least one mesh in the scene
    // All the (sub)meshes should have the same vertex format
    const vertex_format_descriptor_type vertex_format_descriptor(get_vertex_format(scene->mMeshes[0]));
    size_t vertex_data_size = 0;
    size_t index_data_size = 0;
    for(int index = 0; index < scene->mNumMeshes; ++index)
    {
        const aiMesh* mesh = scene->mMeshes[index];
        const vertex_format_descriptor_type descriptor(get_vertex_format(scene->mMeshes[index]));

        if(descriptor != vertex_format_descriptor)
        {
            throw exception_type("All meshes in a scene must have the same vertex format! Input file: {}", path);
        }

        vertex_data_size += get_vertex_data_size(mesh, descriptor);
        index_data_size += get_index_data_size(mesh);
    }
    std::vector<uint8_t> vertex_data;
    vertex_data.reserve(vertex_data_size);
    std::vector<uint16_t> index_data;
    index_data.reserve(index_data_size);

    _submesh_data.reserve(scene->mNumMeshes);
    _materials.reserve(scene->mNumMaterials);

    uint16_t vertex_offset = 0;
    uint16_t index_offset = 0;
    for(int index = 0; index < scene->mNumMeshes; ++index)
    {
        const aiMesh* mesh = scene->mMeshes[index];
        std::vector<uint8_t> mesh_vertex_data = get_vertex_data(mesh, vertex_format_descriptor);
        std::vector<uint16_t> mesh_index_data = ::get_index_data(mesh);
        _submesh_data.emplace_back(vertex_offset,
            static_cast<uint16_t>(mesh_vertex_data.size()),
            index_offset,
            static_cast<uint16_t>(mesh_index_data.size()),
            mesh->mMaterialIndex);
        std::transform(mesh_index_data.cbegin(), mesh_index_data.cend(), mesh_index_data.begin(), [vertex_count = vertex_offset / vertex_format_descriptor.get_stride_size()](uint16_t value) -> uint16_t
        {
            return value + vertex_count;
        });
        vertex_offset += mesh_vertex_data.size();
        index_offset += mesh_index_data.size();
        vertex_data.insert(vertex_data.end(), mesh_vertex_data.begin(), mesh_vertex_data.end());
        index_data.insert(index_data.end(), mesh_index_data.begin(), mesh_index_data.end());
    }

    _mesh_data.vertex_format_descriptor = vertex_format_descriptor;
    _mesh_data.vertex_data = std::move(vertex_data);
    _mesh_data.index_data = std::move(index_data);
}

std::vector<vk::VertexInputAttributeDescription> yoe::mesh_type::get_vertex_attribute_descriptions() const
{
    std::vector<vk::VertexInputAttributeDescription> result;

    result.reserve(_mesh_data.vertex_format_descriptor.get_vertex_format().size());
    for(const vertex_format_type& vertex_format: _mesh_data.vertex_format_descriptor.get_vertex_format())
    {
        vk::VertexInputAttributeDescription vertex_input_attribute_description;

        // Binding is always 0 here, we have only one buffer to provide all the data
        vertex_input_attribute_description.binding = 0;
        vertex_input_attribute_description.location = get_location(vertex_format);
        vertex_input_attribute_description.format = convert(vertex_format);
        vertex_input_attribute_description.offset = _mesh_data.vertex_format_descriptor.get_offset(vertex_format);

        result.push_back(vertex_input_attribute_description);

    }

    return result;
}

size_t yoe::mesh_type::get_stride_size() const
{
    return _mesh_data.vertex_format_descriptor.get_stride_size();
}
