#include <yoe/assets/material.hh>

yoe::material_type::material_type() = default;

yoe::material_type::material_type(std::vector<renderer_type::image_handle_type> images):
    _images(std::move(images))
{
    // Nothing to do yet
}

void yoe::material_type::add_image(renderer_type::image_handle_type image)
{
    _images.push_back(image);
}
