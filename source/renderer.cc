#include <map>
#include <yoe/renderer.hh>
#include <vector>
#include <set>
#include <yoe/logger.hh>
#include <yoe/assets/image.hh>
#include <yoe/assets/mesh.hh>
#include <yoe/spirv/reflection.hh>

#include <glm/gtc/matrix_transform.hpp>
// #define GLM_FORCE_ALIGNED_GENTYPES
// #include <glm/gtc/type_aligned.hpp>

namespace
{
    template<typename T>
    void hash_combine(size_t& seed, const T& value)
    {
        // Taken from boost::hash_combine: https://www.boost.org/doc/libs/1_55_0/doc/html/hash/reference.html#boost.hash_combine
        seed ^= std::hash<T>()(value) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    }

    // TODO: Can we use consts instead of macros?
    // TODO: Group them similarly to the C++ part
    const char* const shader_header = R"(
#version 450

// Vertex attribute locations
#define POSITION_LOCATION 0
#define NORMAL_LOCATION 1
#define UV1_LOCATION 2
#define UV2_LOCATION 3
#define COLOR_LOCATION 4
#define TANGENT_LOCATION 5
#define BITANGENT_LOCATION 6

// Uniform buffer bindings - they are all on set 0
#define UNIFORM_BUFFER_SET 0
#define FRAME_DATA_BINDING 0
#define OBJECT_DATA_BINDING 1

// Texture binginds - they are all on set 1
#define TEXTURE_SET 1
#define ALBEDO_TEXTURE_BINDING 0
#define ALBEDO_SAMPLER_BINDING 1
)";
    struct shader_constants_type
    {
        struct vertex_attribute_location_type
        {
            static constexpr uint32_t position = 0;
            static constexpr uint32_t normal = 1;
            static constexpr uint32_t uv1 = 2;
            static constexpr uint32_t uv2 = 3;
            static constexpr uint32_t color = 4;
            static constexpr uint32_t tangent = 5;
            static constexpr uint32_t bitangent = 6;
        };
        struct uniform_buffer_binding_type
        {
            static constexpr uint32_t set = 0;

            struct frame_data_type
            {
                static constexpr uint32_t binding = 0;
            };
            struct object_data_type
            {
                static constexpr uint32_t binding = 1;
            };
        };
        struct texture_binding_type
        {
            static constexpr uint32_t set = 1;

            struct albedo_type
            {
                struct texture_type
                {
                    static constexpr uint32_t binding = 0;
                };
                struct sampler_type
                {
                    static constexpr uint32_t binding = 1;
                };
            };
        };
    };
    // TODO: Remove this
    constexpr std::array vertex_data = {
        // Position         Colour              UV
        -0.5f, -0.5f, 0.1f,   1.0f, 0.0f, 0.0f,   0.2f, 0.2f,
         0.0f,  0.5f, 0.1f,   0.0f, 1.0f, 0.0f,   0.5f, 0.8f,
         0.5f, -0.5f, 0.1f,   0.0f, 0.0f, 1.0f,   0.8f, 0.2f,
    };

    auto check_extensions(const std::vector<const char*>& required_extension_names, const std::vector<vk::ExtensionProperties>& available_extensions)
    {
        struct result_type
        {
            bool success = true;
            const char* missing_extension = nullptr;
        };

        for(const char* required_extension_name : required_extension_names)
        {
            const size_t required_extension_name_length = strlen(required_extension_name);
            const auto result = std::ranges::find_if(available_extensions, [required_extension_name_length, required_extension_name](const vk::ExtensionProperties& instance_extension)
            {
                return strncmp(instance_extension.extensionName, required_extension_name, required_extension_name_length) == 0;
            });

            if(result == available_extensions.end())
            {
                return result_type {
                    .success = false,
                    .missing_extension = required_extension_name
                };
            }
        }

        return result_type{};
    }

    auto check_layers(const std::vector<const char*>& required_layer_names, const std::vector<vk::LayerProperties>& available_layers)
    {
        struct result_type
        {
            bool success = true;
            const char* missing_layer = nullptr;
        };

        for(const char* required_layer_name : required_layer_names)
        {
            const size_t required_layer_name_length = strlen(required_layer_name);
            const auto result = std::ranges::find_if(available_layers, [required_layer_name_length, required_layer_name](const vk::LayerProperties& instance_layer)
            {
                return strncmp(instance_layer.layerName, required_layer_name, required_layer_name_length) == 0;
            });
            if(result == available_layers.end())
            {
                return result_type {
                    .success = false,
                    .missing_layer = required_layer_name
                };
            }
        }

        return result_type{};
    }

    //
    // Labeling
    //

    // Helper struct to easily define the vk::ObjectType value for a given native vulkan handle
    // It needs tp be specialised for every handle type
    template<typename HandleT>
    struct object_type
    {

    };

    template<>
    struct object_type<VkImage>
    {
        static constexpr vk::ObjectType value = vk::ObjectType::eImage;
    };

    // TODO: There is a isVulkanHandleType defined in vulkan.hpp
    // Concept trying to describe a vk::HandleT, coming from vulkan hpp, e.g. vk::Image.
    // These handles seem to always have these members
    template<typename HandleT>
    concept VkHandleT = requires(HandleT handle)
    {
        typename HandleT::CType;                                                                // Type alias for the native vulkan handle type
        typename HandleT::NativeType;                                                           // Same type alias as above, but with a different name
        std::is_same_v<typename HandleT::CType, typename HandleT::NativeType>;                  // Just to be sure they are the same
        { HandleT::objectType } -> std::convertible_to<vk::ObjectType>;                         // There is a static objectType member of type vk::ObjectType
        { handle.operator typename HandleT::CType() } -> std::same_as<typename HandleT::CType>; // There is a conversion operator to the native vulkan handle type
    };

    // Concept trying to describe a vk::raii::HandleT coming from vulkan raii, e.g. vk::raii::Image.
    // These handles seem to always have these members
    template<typename HandleT>
    concept VkRaiiHandleT = requires(HandleT handle) {
        typename HandleT::CType;                                        // Type alias for the native vulkan handle type
        { HandleT::objectType } -> std::convertible_to<vk::ObjectType>; // There is a static objectType member of type vk::ObjectType
        { *handle } -> std::convertible_to<typename HandleT::CType>;    // There is a dereference operator which returns a vk::HandleT (which is convertible to the vulkan native handle type).
        // TODO: Below is not working, but this would be the best
        //{ *handle } -> std::convertible_to<VkHandleT>;
    };


    // Concept for native vulkan handle types.
    // These handles have nothing specific, they are just pointers esentially,
    // so the best is to make sure they are not vk:: and not vk::raii handle types.
    template<typename HandleT>
    concept NativeHandleT = !VkHandleT<HandleT> && !VkRaiiHandleT<HandleT>;

    // Labeling of native vulkan handles
    template<NativeHandleT HandleT>
    void label_object(HandleT handle, const char* label, const vk::raii::Device& device)
    {
#ifdef YOE_DEBUG
        const vk::DebugUtilsObjectNameInfoEXT debug_utils_object_name_info(object_type<HandleT>::value, reinterpret_cast<uint64_t>(handle), label);
        device.setDebugUtilsObjectNameEXT(debug_utils_object_name_info);
#endif
    }

    // Labeling of vk::HandleT handles
    template<VkHandleT HandleT>
    void label_object(HandleT handle, const char* label, const vk::raii::Device& device)
    {
#ifdef YOE_DEBUG
        const typename HandleT::NativeType native_handle = handle;
        const vk::DebugUtilsObjectNameInfoEXT debug_utils_object_name_info(HandleT::objectType, reinterpret_cast<uint64_t>(native_handle), label);
        device.setDebugUtilsObjectNameEXT(debug_utils_object_name_info);
#endif
    }

    // Labeling of vk::raii::HandleT handles
    template<VkRaiiHandleT HandleT>
    void label_object(const HandleT& handle, const char* label, const vk::raii::Device& device)
    {
#ifdef YOE_DEBUG
        const typename HandleT::CType native_handle = *handle;
        const vk::DebugUtilsObjectNameInfoEXT debug_utils_object_name_info(HandleT::objectType, reinterpret_cast<uint64_t>(native_handle), label);
        device.setDebugUtilsObjectNameEXT(debug_utils_object_name_info);
#endif
    }

    VkBool32 debug_utils_messenger_callback(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity,
        VkDebugUtilsMessageTypeFlagsEXT /*message_types*/,
        const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
        void* /*user_data*/)
    {
        if(message_severity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        {
            yoe::logging::error("Error message from Vulkan: {} ({}) - {}", callback_data->messageIdNumber, callback_data->pMessageIdName, callback_data->pMessage);
        }
        else if(message_severity & VkDebugUtilsMessageSeverityFlagBitsEXT::VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        {
            yoe::logging::warning("Error message from Vulkan: {} ({}) - {}", callback_data->messageIdNumber, callback_data->pMessageIdName, callback_data->pMessage);
        }

        return VK_FALSE;
    }
}

yoe::renderer_type::renderer_type(const char* application_name,
    uint32_t application_version,
    HINSTANCE instance,
    HWND hwnd)
{
    // Check if required instance extensions are available
    const std::vector<const char*> required_instance_extensions = {
        VK_KHR_SURFACE_EXTENSION_NAME,
#ifdef YOE_WINDOWS
        VK_KHR_WIN32_SURFACE_EXTENSION_NAME,
#endif
#ifdef YOE_DEBUG
        "VK_EXT_debug_utils",
#endif
    };
    check_required_instance_extensions_support(required_instance_extensions);

    // Check if required instance layers are available
    const std::vector<const char*> required_layers = {
#ifdef YOE_DEBUG
        "VK_LAYER_KHRONOS_validation",
#endif
    };
    check_required_instance_layers_support(required_layers);

    // Create instance
    const vk::ApplicationInfo application_info(application_name, application_version, "yoe-vulkan", VK_MAKE_VERSION(0, 0, 1), VK_API_VERSION_1_3);
    const vk::InstanceCreateInfo instance_create_info({},
        &application_info,
        static_cast<uint32_t>(required_layers.size()),
        required_layers.data(),
        static_cast<uint32_t>(required_instance_extensions.size()),
        required_instance_extensions.data(),
        nullptr);
    _instance = _context.createInstance(instance_create_info);

#ifdef YOE_DEBUG
    // Set up debug messenger callback: https://docs.vulkan.org/samples/latest/samples/extensions/debug_utils/README.html
    const vk::DebugUtilsMessengerCreateInfoEXT debug_utils_messenger_create_info(vk::DebugUtilsMessengerCreateFlagsEXT(),
        vk::DebugUtilsMessageSeverityFlagBitsEXT::eError | vk::DebugUtilsMessageSeverityFlagBitsEXT::eWarning,
        vk::DebugUtilsMessageTypeFlagBitsEXT::eGeneral | vk::DebugUtilsMessageTypeFlagBitsEXT::ePerformance |
            vk::DebugUtilsMessageTypeFlagBitsEXT::eValidation,
        &debug_utils_messenger_callback);

    _debug_utils_messenger = _instance.createDebugUtilsMessengerEXT(debug_utils_messenger_create_info);
#endif

    // Create surface
    const vk::Win32SurfaceCreateInfoKHR surface_create_info({}, instance, hwnd);
    _surface = _instance.createWin32SurfaceKHR(surface_create_info);

    // Select physical device
    const std::vector<const char*> required_device_extensions = {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME,
        // This does not work in Renderdoc. In RenderDoc the same device has less extensions, see:
        // https://github.com/baldurk/renderdoc/issues/2364
        VK_EXT_MULTI_DRAW_EXTENSION_NAME,
    };

    // Create device and queues
    _physical_device = select_physical_device(required_device_extensions, required_layers);
    _device = create_device(required_device_extensions, required_layers);
    _graphics_queue = _device.getQueue(_physical_device.graphics_queue_family_index, 0);
    _compute_queue = _device.getQueue(_physical_device.compute_queue_family_index, 0);
    _transfer_queue = _device.getQueue(_physical_device.transfer_queue_family_index, 0);
    _presentation_queue = _device.getQueue(_physical_device.presentation_queue_family_index, 0);

    // TODO: How to use vk::raii with VMA?
    _memory_allocator.initialize(*_instance, *_physical_device.physical_device, *_device, VK_API_VERSION_1_3);

    // Create swapchain
    _swapchain = create_swapchain();

    // Create swapchain image views
    _swapchain_image_views.reserve(_swapchain.get_handle().getImages().size());
    uint32_t swapchain_image_views_counter = 0;
    for(const auto& image: _swapchain.get_handle().getImages())
    {
        const vk::ImageSubresourceRange image_subresource_range(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);
        const vk::ImageViewCreateInfo image_view_create_info(vk::ImageViewCreateFlags(),
            image,
            vk::ImageViewType::e2D,
            convert_enum<vk::Format>(_swapchain.get_create_info().imageFormat),
            vk::ComponentMapping(),
            image_subresource_range);

        _swapchain_image_views.emplace_back(_device.createImageView(image_view_create_info));

        const std::string label = "Swapchain Image View " + std::to_string(swapchain_image_views_counter++);
        label_object(_swapchain_image_views.back(), label.c_str(), _device);
    }

    // Create depth image
    const VkExtent3D extent = {
        .width = _swapchain.get_create_info().imageExtent.width,
        .height = _swapchain.get_create_info().imageExtent.height,
        .depth = 1,
    };
    const VkImageCreateInfo depth_image_create_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = VK_FORMAT_D32_SFLOAT_S8_UINT,
        .extent = extent,
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED
    };
    _depth_image = _memory_allocator.create_image(depth_image_create_info);
    label_object(_depth_image.get_handle(), "Depth Texture", _device);

    // Create depth image view
    const vk::ImageSubresourceRange depth_image_subresource_range(vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil, 0, 1, 0, 1);
    const vk::ImageViewCreateInfo depth_image_view_create_info(vk::ImageViewCreateFlags(),
        _depth_image.get_handle(),
        vk::ImageViewType::e2D,
        convert_enum<vk::Format>(depth_image_create_info.format),
        vk::ComponentMapping(),
        depth_image_subresource_range);
    _depth_image_view = _device.createImageView(depth_image_view_create_info);

    // Create render pass
    const std::array render_pass_attachment_descriptions = {
        // Colour attachment
        vk::AttachmentDescription(vk::AttachmentDescriptionFlags{},
            convert_enum<vk::Format>(_swapchain.get_create_info().imageFormat),
            vk::SampleCountFlagBits::e1,
            vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore,
            vk::AttachmentLoadOp::eDontCare,
            vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined,
            vk::ImageLayout::ePresentSrcKHR),
        // Depth attachment
        vk::AttachmentDescription(vk::AttachmentDescriptionFlags{},
            vk::Format::eD32SfloatS8Uint,
            vk::SampleCountFlagBits::e1,
            vk::AttachmentLoadOp::eClear,
            vk::AttachmentStoreOp::eStore,
            vk::AttachmentLoadOp::eDontCare,
            vk::AttachmentStoreOp::eDontCare,
            vk::ImageLayout::eUndefined,
            vk::ImageLayout::eDepthStencilAttachmentOptimal),
    };
    constexpr vk::AttachmentReference colour_attachment_reference(0, vk::ImageLayout::eColorAttachmentOptimal);
    constexpr vk::AttachmentReference depth_attachment_reference(1, vk::ImageLayout::eDepthStencilAttachmentOptimal);
    const  vk::SubpassDescription subpass_description(vk::SubpassDescriptionFlags{},
        vk::PipelineBindPoint::eGraphics,
        0,
        nullptr,
        1,
        &colour_attachment_reference,
        nullptr,
        &depth_attachment_reference,
        0,
        nullptr);


    const vk::SubpassDependency subpass_dependency(VK_SUBPASS_EXTERNAL,
        0,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::PipelineStageFlagBits::eColorAttachmentOutput,
        vk::AccessFlagBits::eNone,
        vk::AccessFlagBits::eColorAttachmentWrite);
    // TODO: Define VULKAN_HPP_NO_STRUCT_CONSTRUCTORS, so then we can have nicer intialization
    const vk::RenderPassCreateInfo render_pass_create_info(vk::RenderPassCreateFlags{},
        static_cast<uint32_t>(render_pass_attachment_descriptions.size()),
        render_pass_attachment_descriptions.data(),
        1,
        &subpass_description,
        1,
        &subpass_dependency);

    _render_pass = _device.createRenderPass(render_pass_create_info);

    // Create framebuffers
    _framebuffers.reserve(_swapchain_image_views.size());
    for(const auto& image_view: _swapchain_image_views)
    {
        std::array attachments = {
            *image_view,
            *_depth_image_view
        };
        const vk::FramebufferCreateInfo framebuffer_create_info(vk::FramebufferCreateFlags(),
            *_render_pass,
            2,
            attachments.data(),
            _swapchain.get_create_info().imageExtent.width,
            _swapchain.get_create_info().imageExtent.height,
            1);

        _framebuffers.emplace_back(_device.createFramebuffer(framebuffer_create_info));
    }

    // Create semaphores
    constexpr auto semaphore_create_info = vk::SemaphoreCreateInfo(vk::SemaphoreCreateFlags());
    for(size_t index = 0; index < _swapchain_image_views.size(); ++index)
    {
        _swapcain_semaphores.emplace_back(_device.createSemaphore(semaphore_create_info));
        _rendering_semaphores.emplace_back(_device.createSemaphore(semaphore_create_info));
    }

    // Create command pools
    const vk::CommandPoolCreateInfo graphics_command_pool_create_info(vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
        _physical_device.graphics_queue_family_index);
    _graphics_command_pool = _device.createCommandPool(graphics_command_pool_create_info);

    const vk::CommandPoolCreateInfo transfer_command_pool_create_info(vk::CommandPoolCreateFlagBits::eResetCommandBuffer,
        _physical_device.transfer_queue_family_index);
    _transfer_command_pool = _device.createCommandPool(transfer_command_pool_create_info);

    // Create graphics command buffers
    const vk::CommandBufferAllocateInfo graphics_command_buffer_allocate_info(*_graphics_command_pool,
        vk::CommandBufferLevel::ePrimary,
        static_cast<uint32_t>(_swapchain_image_views.size()));
    _graphics_command_buffers = _device.allocateCommandBuffers(graphics_command_buffer_allocate_info);

    // Create tranfer command pool
    const vk::CommandBufferAllocateInfo transfer_command_buffer_allocate_info(*_transfer_command_pool,
        vk::CommandBufferLevel::ePrimary,
        1);
    _transfer_command_buffer = std::move(_device.allocateCommandBuffers(transfer_command_buffer_allocate_info)[0]);

    // Create grapchic fences
    constexpr vk::FenceCreateInfo fence_create_info(vk::FenceCreateFlagBits::eSignaled);
    for(size_t index = 0; index < _swapchain_image_views.size(); ++index)
    {
        _graphics_command_buffer_fences.emplace_back(_device.createFence(fence_create_info));
    }

    // Create pipeline

    // Create shader modules
    const char* vertex_shader_code = R"(
layout(location = POSITION_LOCATION) in vec3 inPosition;
layout(location = COLOR_LOCATION) in vec3 inColor;
layout(location = UV1_LOCATION) in vec2 inUv;

layout(location = 0) out vec3 color;
layout(location = 1) out vec2 uv;

layout(set = UNIFORM_BUFFER_SET, binding = FRAME_DATA_BINDING) uniform FrameData {
    vec3 cameraPosition;
    vec3 cameraDirection;
} frameData;

layout(set = UNIFORM_BUFFER_SET, binding = OBJECT_DATA_BINDING) uniform ObjectData {
    mat4 model;
    mat4 view;
    mat4 proj;
} objectData;

void main() {
    gl_Position = objectData.proj * objectData.view * objectData.model * vec4(inPosition, 1.0);
    color = inColor;
    uv = inUv;
})";
    const std::array vertex_shader_full_code = {
        shader_header,
        vertex_shader_code
    };
    const std::vector<uint32_t> vertex_shader_spirv_code = compiler_type::compile(compiler_type::shader_kind::VERTEX,
        vertex_shader_full_code.data(),
        vertex_shader_full_code.size());
    const vk::raii::ShaderModule vertex_shader_module =
        create_shader_module(compiler_type::shader_kind::VERTEX, vertex_shader_spirv_code);

    const char* fragment_shader_code = R"(
layout(location = 0) in vec3 color;
layout(location = 1) in vec2 uv;

// See: https://stackoverflow.com/questions/77070602/how-are-separated-sampled-images-and-samplers-used-in-vulkan-with-glsl
layout(set = TEXTURE_SET, binding = ALBEDO_TEXTURE_BINDING) uniform texture2D inTexture;
layout(set = TEXTURE_SET, binding = ALBEDO_SAMPLER_BINDING) uniform sampler inSampler;

layout(location = 0) out vec4 outColor;

layout(set = UNIFORM_BUFFER_SET, binding = OBJECT_DATA_BINDING) uniform ObjectData {
    mat4 model;
    mat4 view;
    mat4 proj;
} objectData;

void main() {
    vec4 textureColor = texture(sampler2D(inTexture, inSampler), uv);
    outColor = textureColor;
    // outColor = vec4(1.0, 1.0, 1.0, 1.0) * vec4(color, 1.0);
    
})";
    const std::array fragment_shader_full_code = {
        shader_header,
        fragment_shader_code
    };
    const std::vector<uint32_t> fragment_shader_spirv_code = compiler_type::compile(compiler_type::shader_kind::FRAGMENT,
        fragment_shader_full_code.data(),
        fragment_shader_full_code.size());
    vk::raii::ShaderModule const fragment_shader_module =
        create_shader_module(compiler_type::shader_kind::FRAGMENT, fragment_shader_spirv_code);

    std::array pipeline_shader_stage_create_infos = {
        vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(), vk::ShaderStageFlagBits::eVertex, *vertex_shader_module, "main"),
        vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(), vk::ShaderStageFlagBits::eFragment, *fragment_shader_module, "main"),
    };

    // Simple vertex input with one binding (one vertex buffer)
    constexpr std::array vertex_input_binding_descriptions = {
        vk::VertexInputBindingDescription(0, 8 * sizeof(float)),
    };
    constexpr std::array vertex_input_attribute_descriptions = {
        vk::VertexInputAttributeDescription(shader_constants_type::vertex_attribute_location_type::position,
            0,
            vk::Format::eR32G32B32Sfloat,
            0),                 // Position
        vk::VertexInputAttributeDescription(shader_constants_type::vertex_attribute_location_type::color,
            0,
            vk::Format::eR32G32B32Sfloat,
            3 * sizeof(float)), // Colour
        vk::VertexInputAttributeDescription(shader_constants_type::vertex_attribute_location_type::uv1,
            0,
            vk::Format::eR32G32Sfloat,
            6 * sizeof(float)),    // UV
    };
    const vk::PipelineVertexInputStateCreateInfo vertex_input_state_create_info(vk::PipelineVertexInputStateCreateFlags{},
        vertex_input_binding_descriptions.size(),
        vertex_input_binding_descriptions.data(),
        vertex_input_attribute_descriptions.size(),
        vertex_input_attribute_descriptions.data()
    );

    // Input is a triangle list
    constexpr vk::PipelineInputAssemblyStateCreateInfo input_assembly_state_create_info(vk::PipelineInputAssemblyStateCreateFlags{},
            vk::PrimitiveTopology::eTriangleList);

    // No tessellation
    constexpr vk::PipelineTessellationStateCreateInfo tessellation_state_create_info(vk::PipelineTessellationStateCreateFlags{},
        0);

    // Full viewport
    const vk::Viewport viewport(0.0,
        0.0,
        static_cast<float>(_swapchain.get_create_info().imageExtent.width),
        static_cast<float>(_swapchain.get_create_info().imageExtent.height),
        0.0f,
        1.0f);
    const vk::Rect2D scissor(vk::Offset2D(0, 0),
        vk::Extent2D(_swapchain.get_create_info().imageExtent.width, _swapchain.get_create_info().imageExtent.height));
    const vk::PipelineViewportStateCreateInfo viewport_state_create_info(vk::PipelineViewportStateCreateFlags{},
        1,
        &viewport,
        1,
        &scissor);

    // Fill rasterization with backface culling
    constexpr vk::PipelineRasterizationStateCreateInfo rasterization_state_create_info(vk::PipelineRasterizationStateCreateFlags{},
        VK_FALSE,
        VK_FALSE,
        vk::PolygonMode::eFill,
        vk::CullModeFlagBits::eBack,
        vk::FrontFace::eCounterClockwise,
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f);

    // No multisampling
    constexpr vk::PipelineMultisampleStateCreateInfo multisample_state_create_info(vk::PipelineMultisampleStateCreateFlags{},
        vk::SampleCountFlagBits::e1); // The rest is defaulted

    // Depth test/write enabled, stencil test/write disabled
    constexpr vk::PipelineDepthStencilStateCreateInfo depth_stencil_state_create_info(vk::PipelineDepthStencilStateCreateFlags{},
        VK_TRUE,
        VK_TRUE,
        vk::CompareOp::eLessOrEqual); // The rest is defautled

    // Disable blending
    constexpr vk::PipelineColorBlendAttachmentState color_blend_attachment_state(VK_FALSE,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

    const vk::PipelineColorBlendStateCreateInfo color_blend_state_create_info(vk::PipelineColorBlendStateCreateFlags(),
        VK_FALSE,
        vk::LogicOp::eCopy,
        1,
        &color_blend_attachment_state);

    // No dynamic state
    constexpr vk::PipelineDynamicStateCreateInfo dynamic_state_create_info;

    // Create pipeline layout
    const std::array descriptor_set_layout_bindings0 = {
        vk::DescriptorSetLayoutBinding(shader_constants_type::uniform_buffer_binding_type::object_data_type::binding,
            vk::DescriptorType::eUniformBuffer,
            1,
            vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment,
            nullptr),
    };
    const std::array descriptor_set_layout_bindings1 = {
        vk::DescriptorSetLayoutBinding(shader_constants_type::texture_binding_type::albedo_type::sampler_type::binding,
            vk::DescriptorType::eSampler,
            1,
            vk::ShaderStageFlagBits::eFragment,
            nullptr),
        vk::DescriptorSetLayoutBinding(shader_constants_type::texture_binding_type::albedo_type::texture_type::binding,
            vk::DescriptorType::eSampledImage,
            1,
            vk::ShaderStageFlagBits::eFragment,
            nullptr)
    };

    const vk::DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info0(vk::DescriptorSetLayoutCreateFlags{},
        static_cast<uint32_t>(descriptor_set_layout_bindings0.size()),
        descriptor_set_layout_bindings0.data());
    const vk::DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info1(vk::DescriptorSetLayoutCreateFlags{},
        static_cast<uint32_t>(descriptor_set_layout_bindings1.size()),
        descriptor_set_layout_bindings1.data());

    _descriptor_set_layout0 = _device.createDescriptorSetLayout(descriptor_set_layout_create_info0);
    _descriptor_set_layout1 = _device.createDescriptorSetLayout(descriptor_set_layout_create_info1);
    std::array<const vk::DescriptorSetLayout, 2> descriptor_set_layout_array = {
        *_descriptor_set_layout0,
        *_descriptor_set_layout1,
    };

    const vk::PipelineLayoutCreateInfo pipeline_layout_create_info(vk::PipelineLayoutCreateFlags{},
        static_cast<uint32_t>(descriptor_set_layout_array.size()),
        descriptor_set_layout_array.data());
    _graphics_pipeline_layout = _device.createPipelineLayout(pipeline_layout_create_info);

    // Finally create the pipeline itself
    const vk::GraphicsPipelineCreateInfo graphics_pipeline_create_info(vk::PipelineCreateFlags{},
        pipeline_shader_stage_create_infos.size(),
        pipeline_shader_stage_create_infos.data(),
        &vertex_input_state_create_info,
        &input_assembly_state_create_info,
        &tessellation_state_create_info,
        &viewport_state_create_info,
        &rasterization_state_create_info,
        &multisample_state_create_info,
        &depth_stencil_state_create_info,
        &color_blend_state_create_info,
        &dynamic_state_create_info,
        *_graphics_pipeline_layout,
        *_render_pass,
        0); // No base pipeline
    _graphics_pipeline = _device.createGraphicsPipeline(nullptr, graphics_pipeline_create_info);

    // Create vertex buffer
    _vertex_buffer = create_buffer(vertex_data.size() * sizeof(float), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, false, vertex_data.data());

    // Initialize uniform buffer data
    //_uniform_buffer_data0.model = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    _uniform_buffer_data0.view = glm::lookAt(glm::vec3(0.0f, 0.0f, -2.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    // TODO: Resolution and then aspect ratio is hard coded
    _uniform_buffer_data0.projection = glm::perspective(glm::radians(90.0f), 1920.0f / 1080.0f, 0.1f, 100.0f);
    _uniform_buffer_data1 = _uniform_buffer_data0;

    // Create uniform buffers
    _uniform_buffers.reserve(_swapchain_image_views.size());
    _uniform_buffer_mappings.reserve(_swapchain_image_views.size());
    for(size_t index = 0; index < _swapchain_image_views.size(); ++index)
    {
        // Two uniform buffers for each mesh
        _uniform_buffers.emplace_back(create_buffer(sizeof(object_data_uniform_buffer_type), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true));
        _uniform_buffer_mappings.emplace_back(_uniform_buffers.back().map_memory());
        void* pointer1 = _uniform_buffer_mappings.back().get_pointer();
        memcpy(pointer1, &_uniform_buffer_data0, sizeof(object_data_uniform_buffer_type));

        _uniform_buffers.emplace_back(create_buffer(sizeof(object_data_uniform_buffer_type), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true));
        _uniform_buffer_mappings.emplace_back(_uniform_buffers.back().map_memory());
        void* pointer2 = _uniform_buffer_mappings.back().get_pointer();
        memcpy(pointer2, &_uniform_buffer_data1, sizeof(object_data_uniform_buffer_type));
    }

    // Create descriptor sets
    const std::array descriptor_pool_sizes = {
        vk::DescriptorPoolSize(vk::DescriptorType::eUniformBuffer, static_cast<uint32_t>(_swapchain_image_views.size()) * 2),
        vk::DescriptorPoolSize(vk::DescriptorType::eSampler, 2),
        vk::DescriptorPoolSize(vk::DescriptorType::eSampledImage, 2),
    };
    const vk::DescriptorPoolCreateInfo descriptor_pool_create_info(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
        static_cast<uint32_t>(_swapchain_image_views.size()) * 2 + 2,
        static_cast<uint32_t>(descriptor_pool_sizes.size()),
        descriptor_pool_sizes.data());
    _descriptor_pool = _device.createDescriptorPool(descriptor_pool_create_info);

    // Load mesh
    shader_handle_type shader = create_shader(vertex_shader_code, fragment_shader_code);
    {
        image_handle_type image = create_image("images/color_grid.png");
        material_handle_type material = create_material(shader, image);
        std::ignore = create_mesh(mesh_type("meshes/test.obj"), material, glm::vec3(2.0f, 3.0f, 5.0f));
    }
    {
        image_handle_type image = create_image("images/uv_grid.png");
        material_handle_type material = create_material(shader, image);
        std::ignore = create_mesh(mesh_type("meshes/test.obj"), material, glm::vec3(-2.0f, 3.0f, 5.0f));
    }


    // Create images
    std::ignore = create_image("images/color_grid.png");
    std::ignore = create_image("images/uv_grid.png");

    // Update descriptor sets
    // Since we are allocate more descriptr sets in one go, we need an array for the required descriptor set layouts
    // even if all of them are the same
    {
        const std::vector descriptor_set_layouts(_swapchain_image_views.size() * 2, *_descriptor_set_layout0);
        const vk:: DescriptorSetAllocateInfo descriptor_set_allocate_info(*_descriptor_pool,
            static_cast<uint32_t>(descriptor_set_layouts.size()),
            descriptor_set_layouts.data());
        _descriptor_sets0 = _device.allocateDescriptorSets(descriptor_set_allocate_info);
    }

    for(size_t index = 0; index < _swapchain_image_views.size(); ++index)
    {
        const vk::DescriptorBufferInfo descriptor_buffer_info0(_uniform_buffers[index].get_handle(),
            0,
            sizeof(object_data_uniform_buffer_type));
        const vk::WriteDescriptorSet buffer_write_descriptor_set0(*_descriptor_sets0[index],
            shader_constants_type::uniform_buffer_binding_type::object_data_type::binding,
            0,
            1,
            vk::DescriptorType::eUniformBuffer,
            nullptr,
            &descriptor_buffer_info0,
            nullptr);
        const vk::DescriptorBufferInfo descriptor_buffer_info1(_uniform_buffers[index + _swapchain_image_views.size()].get_handle(),
            0,
            sizeof(object_data_uniform_buffer_type));
        const vk::WriteDescriptorSet buffer_write_descriptor_set1(*_descriptor_sets0[index + _swapchain_image_views.size()],
            shader_constants_type::uniform_buffer_binding_type::object_data_type::binding,
            0,
            1,
            vk::DescriptorType::eUniformBuffer,
            nullptr,
            &descriptor_buffer_info1,
            nullptr);
        _device.updateDescriptorSets(
             { buffer_write_descriptor_set0, buffer_write_descriptor_set1 },
             {});
    }
    // For set 1 we create two descriptor sets for the two images
    {
        const std::vector descriptor_set_layouts(2, *_descriptor_set_layout1);
        const vk:: DescriptorSetAllocateInfo descriptor_set_allocate_info(*_descriptor_pool,
            static_cast<uint32_t>(descriptor_set_layouts.size()),
            descriptor_set_layouts.data());
        _descriptor_sets1 = _device.allocateDescriptorSets(descriptor_set_allocate_info);
    }
    for(uint32_t index = 0; index < 2; ++index)
    {
        const vk::DescriptorImageInfo descriptor_image_info(*_images[index].sampler, *_images[index].view, vk::ImageLayout::eShaderReadOnlyOptimal);
        const vk::WriteDescriptorSet sampler_write_descriptor_set(*_descriptor_sets1[index],
            shader_constants_type::texture_binding_type::albedo_type::sampler_type::binding,
            0,
            1,
            vk::DescriptorType::eSampler,
            &descriptor_image_info,
            nullptr,
            nullptr);
        const vk::WriteDescriptorSet image_write_descriptor_set(*_descriptor_sets1[index],
            shader_constants_type::texture_binding_type::albedo_type::texture_type::binding,
            0,
            1,
            vk::DescriptorType::eSampledImage,
            &descriptor_image_info,
            nullptr,
            nullptr);

        _device.updateDescriptorSets(
            { sampler_write_descriptor_set, image_write_descriptor_set },
            {});
    }

}

yoe::renderer_type::~renderer_type()
{
    _device.waitIdle();
}

void yoe::renderer_type::run()
{
    time_point_type now = clock_type::now();
    _delta_time = std::chrono::duration_cast<seconds_type>(now - _previous_time);
    _previous_time = now;
    // Wait for the command buffer to finish
    std::initializer_list<vk::Fence> fences_list = { *_graphics_command_buffer_fences[_current_frame] };
    const vk::ArrayProxy fences = fences_list;

    if(_device.waitForFences(fences, VK_TRUE, UINT64_MAX) != vk::Result::eSuccess)
    {
        throw exception_type("Failed to wait for fence!");
    }
    _device.resetFences(fences);

    // Get image to draw to
    const auto [result, index] = _swapchain.get_handle().acquireNextImage(std::numeric_limits<uint64_t>::max(), *_swapcain_semaphores[_current_frame]);
    if(result == vk::Result::eTimeout)
    {
        return;
    }
    if(result == vk::Result::eSuboptimalKHR || result == vk::Result::eErrorOutOfDateKHR)
    {
        // TODO: Recreate swapchain
        return;
    }
    if(result == vk::Result::eNotReady)
    {
        return;
    }
    if(result != vk::Result::eSuccess)
    {
        throw exception_type("Failed to aquire image from swapchain!");
    }

    // Update uniform data
    //_uniform_buffer_data.model = glm::rotate(_uniform_buffer_data.model, glm::radians(0.05f) * static_cast<float>(_current_frame), glm::vec3(1.0f, 0.0f, 0.0f));
    const float x = glm::sin(glm::radians(0.05f) * static_cast<float>(_frame_index));
    // _uniform_buffer_data0.model[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f); // This is ho to reset the transform in the matrix
    const auto translate = glm::translate(glm::mat4(1.0f), glm::vec3(x, 0.0f, 0.0f));
    const auto rotate = glm::rotate(glm::mat4(1.0f), glm::radians(0.05f) * static_cast<float>(_frame_index), glm::vec3(0.0f, 0.0f, 1.0f));
    _uniform_buffer_data0.model = translate * rotate;
    void* pointer1 =_uniform_buffer_mappings[_current_frame].get_pointer();
    memcpy(pointer1, &_uniform_buffer_data0, sizeof(object_data_uniform_buffer_type));

    _uniform_buffer_data1.model = glm::translate(glm::mat4(1.0f), glm::vec3(-x, 0.0f, 0.0f));
    void* pointer2 =_uniform_buffer_mappings[_current_frame + _swapchain_image_views.size()].get_pointer();
    memcpy(pointer2, &_uniform_buffer_data1, sizeof(object_data_uniform_buffer_type));

    constexpr vk::CommandBufferBeginInfo command_buffer_begin_info;
    const vk::raii::CommandBuffer& command_buffer = _graphics_command_buffers[_current_frame];
    command_buffer.begin(command_buffer_begin_info);

    const vk::Rect2D render_area(vk::Offset2D(0, 0), _swapchain.get_create_info().imageExtent);
    constexpr std::array clear_values = {
        vk::ClearValue(vk::ClearColorValue(std::array{ 0.0f, 1.0f, 0.0f, 1.0f })),
        vk::ClearValue(vk::ClearDepthStencilValue(1.0f, 0))
    };
    const vk::RenderPassBeginInfo render_pass_begin_info(*_render_pass,
        *_framebuffers[_current_frame],
        render_area,
        clear_values.size(),
        clear_values.data()
        );

    // Beginning the render pass should clear the image
    command_buffer.beginRenderPass(render_pass_begin_info, vk::SubpassContents::eInline);

    // TODO: Add this back
    for(auto& mesh: _meshes)
    {
        // TODO: Fetch graphics pipeline for mesh
        const material_data_type& material = _materials[mesh._material._value];
        const shader_data_type& shader = _shaders[material._shader._value];
        std::vector<vk::DescriptorSet> descriptor_sets;
        descriptor_sets.reserve(mesh._descriptor_sets.size());
        // The first [0.._swapchain_image_views.size()] items are for the uniform buffers, and we select the one corresponding to the current frame
        descriptor_sets.push_back(*mesh._descriptor_sets[_current_frame]);
        // The rest of the descriptor sets are just need to be converted from vk::raii::DescriptorSet to vk::DescriptorSet
        std::transform(mesh._descriptor_sets.begin() + _swapchain_image_views.size(), mesh._descriptor_sets.end(), std::back_inserter(descriptor_sets), [](const vk::raii::DescriptorSet& descriptor_set)
        {
            return *descriptor_set;
        });
        // Update object uniform data, just for fun
        mesh._object_data_uniform_buffer.model = glm::rotate(mesh._object_data_uniform_buffer.model,
            glm::radians(360.0f) * _delta_time.count(),
            glm::vec3(0.0f, 1.0f, 0.0f));
        auto mapped_memory = mesh._object_data_uniform_buffers[_current_frame].map_memory();
        memcpy(mapped_memory.get_pointer(), &mesh._object_data_uniform_buffer, sizeof(object_data_uniform_buffer_type));
        command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, *_pipelines[mesh._pipeline._value]);
        command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *shader._pipeline_layout, 0, descriptor_sets, {});
        command_buffer.bindVertexBuffers(0, { mesh._vertex_buffer.get_handle() }, { 0 });
        command_buffer.bindIndexBuffer(mesh._index_buffer.get_handle(), 0, vk::IndexType::eUint16);
        command_buffer.drawIndexed(mesh._index_count, 1, 0, 0, 0);
    }

    command_buffer.bindPipeline(vk::PipelineBindPoint::eGraphics, *_graphics_pipeline);
    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *_graphics_pipeline_layout, 0, { *_descriptor_sets0[_current_frame], *_descriptor_sets1[0] }, {});
    command_buffer.bindVertexBuffers(0, { _vertex_buffer.get_handle() }, { 0 });
    command_buffer.draw(vertex_data.size() / 8, 1, 0, 0);

    command_buffer.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, *_graphics_pipeline_layout, 0, { *_descriptor_sets0[_current_frame + _swapchain_image_views.size()], *_descriptor_sets1[1] }, {});
    command_buffer.draw(vertex_data.size() / 8, 1, 0, 0);

    command_buffer.endRenderPass();

    command_buffer.end();

    // Submit command buffer
    constexpr  vk::PipelineStageFlags wait_stage_mask = vk::PipelineStageFlagBits::eTopOfPipe;
    const vk::SubmitInfo submit_info(1,
        &*_swapcain_semaphores[_current_frame],
        &wait_stage_mask,
        1,
        &*command_buffer,
        1,
        &*_rendering_semaphores[_current_frame]);
    _graphics_queue.submit(submit_info, fences.front());

    // Present to the surface
    const vk::PresentInfoKHR present_info(1,
        &*_rendering_semaphores[_current_frame],
        1,
        &*_swapchain.get_handle(),
        &index);
    const vk::Result present_result = _presentation_queue.presentKHR(present_info);

    if(present_result == vk::Result::eSuboptimalKHR || present_result == vk::Result::eErrorOutOfDateKHR)
    {
        // TODO: Recreate swapchain
        return;
    }
    if(present_result != vk::Result::eSuccess)
    {
        throw exception_type("Failed to present image!");
    }

    _current_frame = (_current_frame + 1) % _swapchain_image_views.size();
    ++_frame_index;
}

yoe::renderer_type::image_handle_type yoe::renderer_type::create_image(const image_type& image)
{
    VkBufferCreateInfo const image_staging_buffer_create_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size = image.get_size(),
        .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1, // The last two element is ignored since sharing mdoe is not concurrent
        .pQueueFamilyIndices = &_physical_device.transfer_queue_family_index,
    };
    const memory_allocator_type::buffer_type image_staging_buffer = _memory_allocator.create_buffer(image_staging_buffer_create_info, true);
    {
        auto mapped_memory = image_staging_buffer.map_memory();
        memcpy(mapped_memory.get_pointer(), image.get_data(), image.get_size());
    }
    const VkFormat image_format = [&image]()
    {
        switch(image.get_format())
        {
            case image_type::format_type::greyscale:
                return VK_FORMAT_R8_SRGB;
            case image_type::format_type::rgb:
                return VK_FORMAT_R8G8B8_SRGB;
            case image_type::format_type::rgba:
                return VK_FORMAT_R8G8B8A8_SRGB;
        }

        // TODO: Error handling
        return VK_FORMAT_R8G8B8A8_SRGB;
    }();
    const VkImageCreateInfo image_create_info = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .imageType = VK_IMAGE_TYPE_2D,
        .format = image_format,
        .extent = {
            .width = image.get_width(),
            .height = image.get_height(),
            .depth = 1,
        },
        .mipLevels = 1,
        .arrayLayers = 1,
        .samples = VK_SAMPLE_COUNT_1_BIT,
        .tiling = VK_IMAGE_TILING_OPTIMAL,
        .usage = VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 0,
        .pQueueFamilyIndices = nullptr,
        .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    };

    yoe::memory_allocator_type::image_type allocated_image = _memory_allocator.create_image(image_create_info);

    // Copy data from staging buffer to image
    vk::CommandBufferBeginInfo const command_buffer_begin_info(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    _transfer_command_buffer.begin(command_buffer_begin_info);

    // Convert image layout from undefined to transfer destination optimal
    const vk::ImageSubresourceRange image_subresource_range(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);
    const vk::ImageMemoryBarrier image_memory_barrier(vk::AccessFlags(),
        vk::AccessFlagBits::eTransferWrite,
        vk::ImageLayout::eUndefined,
        vk::ImageLayout::eTransferDstOptimal,
        VK_QUEUE_FAMILY_IGNORED,
        VK_QUEUE_FAMILY_IGNORED,
        allocated_image.get_handle(),
        image_subresource_range);

    _transfer_command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTopOfPipe,
        vk::PipelineStageFlagBits::eTransfer,
        vk::DependencyFlags(),
        {},
        {},
        { image_memory_barrier });

    // Copy the buffer to the image
    constexpr vk::ImageSubresourceLayers image_subresource_layers(vk::ImageAspectFlagBits::eColor,
        0,
        0,
        1);
    const vk::BufferImageCopy image_copy(0,
        0,
        0,
        image_subresource_layers,
        vk::Offset3D(),
        vk::Extent3D(image.get_width(), image.get_height(), 1));

    _transfer_command_buffer.copyBufferToImage(image_staging_buffer.get_handle(),
                                               allocated_image.get_handle(),
                                               vk::ImageLayout::eTransferDstOptimal,
                                               { image_copy });

    // Convert image layout from transfer destination optimal to shader read only optimal
    const vk::ImageMemoryBarrier image_memory_barrier2(vk::AccessFlagBits::eTransferWrite,
                                                       vk::AccessFlags(),
                                                       vk::ImageLayout::eTransferDstOptimal,
                                                       vk::ImageLayout::eShaderReadOnlyOptimal,
                                                       VK_QUEUE_FAMILY_IGNORED,
                                                       VK_QUEUE_FAMILY_IGNORED,
                                                       allocated_image.get_handle(),
                                                       image_subresource_range);

    _transfer_command_buffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer,
                                             vk::PipelineStageFlagBits::eBottomOfPipe,
                                             vk::DependencyFlags(),
                                             {},
                                             {},
                                             { image_memory_barrier2 });

    _transfer_command_buffer.end();

    vk::SubmitInfo const submit_info(0, nullptr, nullptr, 1, &*_transfer_command_buffer);
    _transfer_queue.submit(submit_info);
    _transfer_queue.waitIdle();

    // Create image views
    const vk::ImageViewCreateInfo image_view_create_info(vk::ImageViewCreateFlags(),
        allocated_image.get_handle(),
        vk::ImageViewType::e2D,
        convert_enum<vk::Format>(image_format),
        vk::ComponentMapping(),
        image_subresource_range);
    vk::raii::ImageView image_view = _device.createImageView(image_view_create_info);

    // Create samplers
    const vk::SamplerCreateInfo sampler_create_info(vk::SamplerCreateFlags(),
        vk::Filter::eLinear,
        vk::Filter::eLinear,
        vk::SamplerMipmapMode::eNearest,
        vk::SamplerAddressMode::eRepeat,
        vk::SamplerAddressMode::eRepeat,
        vk::SamplerAddressMode::eRepeat,
        0,
        VK_FALSE,
        1.0f,
        VK_FALSE,
        vk::CompareOp::eNever,
        0.0f,
        std::numeric_limits<float>::max(),
        vk::BorderColor::eFloatTransparentBlack,
        VK_FALSE);
    vk::raii::Sampler sampler = _device.createSampler(sampler_create_info);

    _images.emplace_back(std::move(allocated_image), std::move(image_view), std::move(sampler));

    return image_handle_type(_images.size() - 1);
}

yoe::renderer_type::mesh_handle_type yoe::renderer_type::create_mesh(const mesh_type& mesh,
    material_handle_type material,
    const glm::vec3& position)
{
    const std::vector<uint8_t>& vertex_data = mesh.get_vertex_data();
    memory_allocator_type::buffer_type vertex_buffer = create_buffer(vertex_data.size(),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        false,
        vertex_data.data());

    const std::vector<uint16_t>& index_data = mesh.get_index_data();
    memory_allocator_type::buffer_type index_buffer = create_buffer(index_data.size() * sizeof(uint16_t),
        VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
        false,
        index_data.data());

    // Create descriptor pool
    const material_data_type& material_data = _materials[material._value];
    const shader_data_type& shader = _shaders[material_data._shader._value];
    std::map<vk::DescriptorType, uint32_t> descriptor_type_counts;
    std::vector<vk::DescriptorPoolSize> descriptor_pool_sizes;
    // Calculate how many sets we need to allocate from the descriptor pool
    uint32_t set_count = 0;

    // Calculate needed sizes
    descriptor_pool_sizes.reserve(shader._descriptor_set_layout_bindings.size());
    for(size_t set_index = 0; set_index < shader._descriptor_set_layout_bindings.size(); ++set_index)
    {
        const std::vector<vk::DescriptorSetLayoutBinding>& descriptor_set_layout_binding =
            shader._descriptor_set_layout_bindings[set_index];
        const bool is_uniform_buffer_binding = set_index == shader_constants_type::uniform_buffer_binding_type::set;

        set_count += is_uniform_buffer_binding
            ? static_cast<uint32_t>(_swapchain_image_views.size())
            : 1;
        for(const auto& binding: descriptor_set_layout_binding)
        {
            descriptor_type_counts[binding.descriptorType] += is_uniform_buffer_binding
                ? binding.descriptorCount * _swapchain_image_views.size() // For uniform buffers we need as many as swapchain images we have
                : binding.descriptorCount;
        }
    }
    for(const auto& descriptor_type_count: descriptor_type_counts)
    {
        const vk::DescriptorPoolSize pool_size(descriptor_type_count.first, descriptor_type_count.second);

        descriptor_pool_sizes.push_back(pool_size);
    }
    const vk::DescriptorPoolCreateInfo descriptor_pool_create_info(vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet,
        set_count, // Max sets
        static_cast<uint32_t>(descriptor_pool_sizes.size()),
        descriptor_pool_sizes.data());
    vk::raii::DescriptorPool descriptor_pool = _device.createDescriptorPool(descriptor_pool_create_info);

    // Allocate descriptor sets
    std::vector<vk::DescriptorSetLayout> descriptor_set_layouts;
    for(size_t set_index = 0; set_index < shader._descriptor_set_layouts.size(); ++set_index)
    {
        const bool is_uniform_buffer_binding = set_index == shader_constants_type::uniform_buffer_binding_type::set;

        if(is_uniform_buffer_binding)
        {
            // For uniform buffers we need as many sets as swapchain images we have
            descriptor_set_layouts.insert(descriptor_set_layouts.end(),
                _swapchain_image_views.size(),
                *shader._descriptor_set_layouts[set_index]);
        }
        else
        {
            descriptor_set_layouts.insert(descriptor_set_layouts.end(),
                *shader._descriptor_set_layouts[set_index]);
        }
    }
    const vk:: DescriptorSetAllocateInfo descriptor_set_allocate_info(*descriptor_pool,
        static_cast<uint32_t>(descriptor_set_layouts.size()),
        descriptor_set_layouts.data());
    std::vector<vk::raii::DescriptorSet> descriptor_sets = _device.allocateDescriptorSets(descriptor_set_allocate_info);

    // Create and initialise uniform buffer data
    frame_data_uniform_buffer_type frame_data_uniform_buffer{};
    object_data_uniform_buffer_type object_data_uniform_buffer{};
    object_data_uniform_buffer.model = glm::translate(object_data_uniform_buffer.model, position);
    object_data_uniform_buffer.model = glm::rotate(object_data_uniform_buffer.model, glm::radians(180.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    object_data_uniform_buffer.view = glm::lookAt(glm::vec3(0.0f, 0.0f, -2.0f), glm::vec3(0.0f), glm::vec3(0.0f, 1.0f, 0.0f));
    // TODO: Resolution and then aspect ratio is hard coded
    object_data_uniform_buffer.projection = glm::perspective(glm::radians(90.0f), 1920.0f / 1080.0f, 0.1f, 100.0f);

    // Update descriptor sets
    std::vector<memory_allocator_type::buffer_type> frame_data_uniform_buffers;
    std::vector<memory_allocator_type::buffer_type> object_data_uniform_buffers;
    std::vector<vk::DescriptorBufferInfo> descriptor_buffer_infos;
    std::vector<vk::DescriptorImageInfo> descriptor_image_infos;
    std::vector<vk::WriteDescriptorSet> buffer_write_descriptor_sets;
    // This variable holds the index of the descriptor_set we are about to update. We are allocating more descriptor sets
    // then the shader declares, since we need to have multiple ones for the uniform buffers
    // So the actual desciptor layout is consisting of multiple descriptor sets
    // One for the actua uniform buffer(s) and one for the texture(s)
    size_t real_set_index = 0;

    descriptor_buffer_infos.reserve(descriptor_set_layouts.size() * 2);
    descriptor_image_infos.reserve(descriptor_set_layouts.size() * 2);
    for(size_t set_index = 0; set_index < shader._descriptor_set_layout_bindings.size(); ++set_index)
    {
        // Update uniform buffer bindings
        if(set_index == shader_constants_type::uniform_buffer_binding_type::set)
        {
            // Creaete one uniform buffer binding for each swapchain image
            for(size_t index = 0; index < _swapchain_image_views.size(); ++index)
            {
                for(size_t binding_index = 0; binding_index < shader._descriptor_set_layout_bindings[set_index].size(); ++binding_index)
                {
                    const vk::DescriptorSetLayoutBinding& binding = shader._descriptor_set_layout_bindings[set_index][binding_index];

                    // Update frame data uniform buffer bindings
                    if(binding.binding == shader_constants_type::uniform_buffer_binding_type::frame_data_type::binding)
                    {
                        // Update frame data unifor buffer bindings
                        // First create uniform buffer
                        // TODO: Should it be constantly mapped
                        frame_data_uniform_buffers.emplace_back(create_buffer(sizeof(frame_data_uniform_buffer_type), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true));
                        descriptor_buffer_infos.emplace_back(frame_data_uniform_buffers.back().get_handle(),
                            0,
                            sizeof(frame_data_uniform_buffer_type));
                        buffer_write_descriptor_sets.emplace_back(*descriptor_sets[real_set_index],
                            static_cast<uint32_t>(binding_index),
                            0,
                            binding.descriptorCount,
                            vk::DescriptorType::eUniformBuffer,
                            nullptr,
                            &descriptor_buffer_infos.back(),
                            nullptr);
                    }
                    if(binding.binding == shader_constants_type::uniform_buffer_binding_type::object_data_type::binding)
                    {
                        // Update object data uniform buffer bindings
                        // First create uniform buffer
                        // TODO: Should it be constantly mapped
                        object_data_uniform_buffers.emplace_back(create_buffer(sizeof(object_data_uniform_buffer_type), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true));

                        // Fill in uniform buffer with data
                        auto mapped_memory = object_data_uniform_buffers.back().map_memory();
                        memcpy(mapped_memory.get_pointer(), &object_data_uniform_buffer, sizeof(object_data_uniform_buffer_type));

                        descriptor_buffer_infos.emplace_back(object_data_uniform_buffers.back().get_handle(),
                            0,
                            sizeof(object_data_uniform_buffer_type));
                        buffer_write_descriptor_sets.emplace_back(*descriptor_sets[real_set_index],
                            static_cast<uint32_t>(binding_index),
                            0,
                            binding.descriptorCount,
                            vk::DescriptorType::eUniformBuffer,
                            nullptr,
                            &descriptor_buffer_infos.back(),
                            nullptr);
                    }
                }

                ++real_set_index;
            }
        }
        if(set_index == shader_constants_type::texture_binding_type::set)
        {
            // Update texture bindings
            for(size_t binding_index = 0; binding_index < shader._descriptor_set_layout_bindings[set_index].size(); ++binding_index)
            {
                const vk::DescriptorSetLayoutBinding& binding = shader._descriptor_set_layout_bindings[set_index][binding_index];
                const image_data_type& albedo_image_data = _images[material_data._albedo_texture._value];

                descriptor_image_infos.emplace_back(*albedo_image_data.sampler,
                    *albedo_image_data.view,
                    vk::ImageLayout::eShaderReadOnlyOptimal);

                // Update sampler bindings
                if(binding.binding == shader_constants_type::texture_binding_type::albedo_type::sampler_type::binding)
                {
                    buffer_write_descriptor_sets.emplace_back(*descriptor_sets[real_set_index],
                        static_cast<uint32_t>(binding_index),
                        0,
                        1,
                        vk::DescriptorType::eSampler,
                        &descriptor_image_infos.back(),
                        nullptr,
                        nullptr);
                }
                // Update texture bindings
                if(binding.binding == shader_constants_type::texture_binding_type::albedo_type::texture_type::binding)
                {
                    buffer_write_descriptor_sets.emplace_back(*descriptor_sets[real_set_index],
                        static_cast<uint32_t>(binding_index),
                        0,
                        1,
                        vk::DescriptorType::eSampledImage,
                        &descriptor_image_infos.back(),
                        nullptr,
                        nullptr);
                }
            }

            ++real_set_index;
        }
    }

    _device.updateDescriptorSets(buffer_write_descriptor_sets, {});

    _meshes.emplace_back(std::move(vertex_buffer),
        mesh.get_vertex_attribute_descriptions(),
        std::move(index_buffer),
        std::move(object_data_uniform_buffers),
        std::move(object_data_uniform_buffer),
        std::move(frame_data_uniform_buffers),
        std::move(frame_data_uniform_buffer),
        static_cast<uint32_t>(index_data.size()),
        static_cast<uint32_t>(mesh.get_stride_size()),
        std::move(descriptor_pool),
        std::move(descriptor_sets),
        material,
        pipeline_handle_type());

    const mesh_handle_type result(_meshes.size() - 1);

    _meshes.back()._pipeline = get_pipeline(result);

    return result;
}

yoe::renderer_type::pipeline_handle_type yoe::renderer_type::get_pipeline(mesh_handle_type mesh_handle)
{
    const size_t hash = get_pipeline_hash(mesh_handle);

    if(_pipeline_map.contains(hash))
    {
        return _pipeline_map[hash];
    }

    return create_pipeline(mesh_handle);
}

yoe::renderer_type::pipeline_handle_type yoe::renderer_type::create_pipeline(mesh_handle_type mesh_handle)
{
    const mesh_data_type& mesh_data = _meshes[mesh_handle._value];
    const material_data_type& material_data = _materials[mesh_data._material._value];
    const shader_data_type& shader_data = _shaders[material_data._shader._value];

    // Simple vertex input with one binding (one vertex buffer)
    const std::array vertex_input_binding_descriptions = {
        vk::VertexInputBindingDescription(0, mesh_data._stride_size, vk::VertexInputRate::eVertex),
    };

    // Calculate vertex input for the pipeline, based on what the mesh has, and what the shader expect
    std::vector<vk::VertexInputAttributeDescription> vertex_input_attribute_descriptions;
    for(const vk::VertexInputAttributeDescription& vertex_input_attribute_description: shader_data._vertex_input_attribute_descriptions)
    {
        const auto mesh_input = std::find_if(mesh_data._vertex_input_attribute_descriptions.begin(),
            mesh_data._vertex_input_attribute_descriptions.end(),
            [&vertex_input_attribute_description](const vk::VertexInputAttributeDescription& value)
            {
                return value.location == vertex_input_attribute_description.location;
            });

        if(mesh_input == mesh_data._vertex_input_attribute_descriptions.end())
        {
            // The mesh does not provide the required vertex attribute
            // TODO: This could be handled by defining additional buffers for the missing vertex attributes
            //       This problem is that it comlicated things a lot, since all these additional buffers need
            //       to be bound when drawing the mesh
            //       Also, Vulkan does not have a constant verttex input rate, like Metal,
            //       the input rate is either vertex or instance. So we cannot have the buffers with only one entry
            //       or instanced draws will fail
            //       so for now we just throw en exception
            throw exception_type("Shader cannot be used with the given mesh! The required vertex attribute {}, is not provided by the mesh!",
                static_cast<std::underlying_type_t<vk::Format>>(vertex_input_attribute_description.format));

        }
        else
        {
            vertex_input_attribute_descriptions.emplace_back(mesh_input->location,
                0,
                mesh_input->format,
                mesh_input->offset);
        }
    }

    const vk::PipelineVertexInputStateCreateInfo vertex_input_state_create_info(vk::PipelineVertexInputStateCreateFlags{},
        vertex_input_binding_descriptions.size(),
        vertex_input_binding_descriptions.data(),
        vertex_input_attribute_descriptions.size(),
        vertex_input_attribute_descriptions.data()
    );

    // Input is a triangle list
    constexpr vk::PipelineInputAssemblyStateCreateInfo input_assembly_state_create_info(vk::PipelineInputAssemblyStateCreateFlags{},
            vk::PrimitiveTopology::eTriangleList);

    // No tessellation
    constexpr vk::PipelineTessellationStateCreateInfo tessellation_state_create_info(vk::PipelineTessellationStateCreateFlags{},
        0);

    // Full viewport
    const vk::Viewport viewport(0.0,
        0.0,
        static_cast<float>(_swapchain.get_create_info().imageExtent.width),
        static_cast<float>(_swapchain.get_create_info().imageExtent.height),
        0.0f,
        1.0f);
    const vk::Rect2D scissor(vk::Offset2D(0, 0),
        vk::Extent2D(_swapchain.get_create_info().imageExtent.width, _swapchain.get_create_info().imageExtent.height));
    const vk::PipelineViewportStateCreateInfo viewport_state_create_info(vk::PipelineViewportStateCreateFlags{},
        1,
        &viewport,
        1,
        &scissor);

    // Fill rasterization with backface culling
    constexpr vk::PipelineRasterizationStateCreateInfo rasterization_state_create_info(vk::PipelineRasterizationStateCreateFlags{},
        VK_FALSE,
        VK_FALSE,
        vk::PolygonMode::eFill,
        vk::CullModeFlagBits::eBack,
        vk::FrontFace::eCounterClockwise,
        VK_FALSE,
        0.0f,
        0.0f,
        0.0f,
        1.0f);

    // No multisampling
    constexpr vk::PipelineMultisampleStateCreateInfo multisample_state_create_info(vk::PipelineMultisampleStateCreateFlags{},
        vk::SampleCountFlagBits::e1); // The rest is defaulted

    // Depth test/write enabled, stencil test/write disabled
    constexpr vk::PipelineDepthStencilStateCreateInfo depth_stencil_state_create_info(vk::PipelineDepthStencilStateCreateFlags{},
        VK_TRUE,
        VK_TRUE,
        vk::CompareOp::eLessOrEqual); // The rest is defautled

    // Disable blending
    constexpr vk::PipelineColorBlendAttachmentState color_blend_attachment_state(VK_FALSE,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::BlendFactor::eOne,
        vk::BlendFactor::eZero,
        vk::BlendOp::eAdd,
        vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA);

    const vk::PipelineColorBlendStateCreateInfo color_blend_state_create_info(vk::PipelineColorBlendStateCreateFlags(),
        VK_FALSE,
        vk::LogicOp::eCopy,
        1,
        &color_blend_attachment_state);

    // No dynamic state
    constexpr vk::PipelineDynamicStateCreateInfo dynamic_state_create_info;

    std::array pipeline_shader_stage_create_infos = {
        vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(), vk::ShaderStageFlagBits::eVertex, *shader_data._vertex_shader , "main"),
        vk::PipelineShaderStageCreateInfo(vk::PipelineShaderStageCreateFlags(), vk::ShaderStageFlagBits::eFragment, *shader_data._fragment_shader, "main"),
    };

    // Finally create the pipeline itself
    const vk::GraphicsPipelineCreateInfo graphics_pipeline_create_info(vk::PipelineCreateFlags{},
        pipeline_shader_stage_create_infos.size(),
        pipeline_shader_stage_create_infos.data(),
        &vertex_input_state_create_info,
        &input_assembly_state_create_info,
        &tessellation_state_create_info,
        &viewport_state_create_info,
        &rasterization_state_create_info,
        &multisample_state_create_info,
        &depth_stencil_state_create_info,
        &color_blend_state_create_info,
        &dynamic_state_create_info,
        *shader_data._pipeline_layout,
        *_render_pass, // TODO: What if we want to use this mesh in a different render pass e.g. the shadow pass
        0); // No base pipeline
    _pipelines.emplace_back(_device.createGraphicsPipeline(nullptr, graphics_pipeline_create_info));

    const size_t hash = get_pipeline_hash(mesh_handle);

    if(_pipeline_map.contains(hash))
    {
        throw exception_type("Internal error! Newly created pipeline has the same hash as a previous one!");
    }

    const pipeline_handle_type result = pipeline_handle_type(_pipelines.size() - 1);

    _pipeline_map[hash] = result;

    return result;
}

namespace
{
    using DescriptorSetLayoutBindingsType = yoe::spirv::reflection_type::DescriptorSetLayoutBindingsType;

    DescriptorSetLayoutBindingsType merge_descriptor_set_layout_bingins(const DescriptorSetLayoutBindingsType& descriptor_set_layout_bindings_a,
        const DescriptorSetLayoutBindingsType& descriptor_set_layout_bindings_b)
    {
        DescriptorSetLayoutBindingsType result;

        const size_t min_set = std::min(descriptor_set_layout_bindings_a.size(), descriptor_set_layout_bindings_b.size());
        // The bindings on the same sets need to be merged
        for(size_t set_index = 0; set_index < min_set; ++set_index)
        {
            result.emplace_back();

            const auto& bindings_a = descriptor_set_layout_bindings_a[set_index];
            const auto& bindings_b = descriptor_set_layout_bindings_b[set_index];
            struct comparator_type
            {
                [[nodiscard]] bool operator()(const vk::DescriptorSetLayoutBinding& binding_a, const vk::DescriptorSetLayoutBinding& binding_b) const
                {
                    // Compare everything EXCEPT the stageFlags - it is OK to have the same binding in different stages
                    return binding_a.binding < binding_b.binding ||
                        binding_a.descriptorCount < binding_b.descriptorCount ||
                        binding_a.pImmutableSamplers < binding_b.pImmutableSamplers ||
                        binding_a.descriptorType < binding_b.descriptorType;
                }
            };
            std::set<vk::DescriptorSetLayoutBinding, comparator_type> merged_bindings(bindings_a.begin(), bindings_a.end());

            merged_bindings.insert(bindings_b.begin(), bindings_b.end());
            result.back().insert(result.back().end(), merged_bindings.begin(), merged_bindings.end());
        }
        // Additional sets can be simply added to the result - one of this for loop will be a noop
        for(size_t set_index = min_set; set_index < descriptor_set_layout_bindings_a.size(); ++set_index)
        {
            result.emplace_back(descriptor_set_layout_bindings_a[set_index]);
        }
        for(size_t set_index = min_set; set_index < descriptor_set_layout_bindings_b.size(); ++set_index)
        {
            result.emplace_back(descriptor_set_layout_bindings_b[set_index]);
        }

        return result;
    }
}

yoe::renderer_type::shader_handle_type yoe::renderer_type::create_shader(const char* vertex_code,
    const char* fragment_code)
{
    using DescriptorSetLayoutBindingsType = spirv::reflection_type::DescriptorSetLayoutBindingsType;

    // Compile shaders
    std::array full_vertex_code = {
        shader_header,
        vertex_code
    };
    const std::vector<uint32_t> vertex_spirv_code = compiler_type::compile(compiler_type::shader_kind::VERTEX,
        full_vertex_code.data(),
        full_vertex_code.size());
    std::array full_fragment_code = {
        shader_header,
        fragment_code
    };
    const std::vector<uint32_t> fragment_spirv_code = compiler_type::compile(compiler_type::shader_kind::FRAGMENT,
        full_fragment_code.data(),
        full_fragment_code.size());
    // Create shader modules
    vk::raii::ShaderModule vertex_shader = create_shader_module(compiler_type::shader_kind::VERTEX, vertex_spirv_code);
    vk::raii::ShaderModule fragment_shader = create_shader_module(compiler_type::shader_kind::FRAGMENT, fragment_spirv_code);
    // Calculate descriptor set layout bindings
    const spirv::reflection_type vertex_reflection(vertex_spirv_code);
    const DescriptorSetLayoutBindingsType vertex_descriptor_set_layout_bindings = vertex_reflection.get_descriptor_set_layout_bindings();
    const std::vector<vk::VertexInputAttributeDescription> vertex_input_attribute_descriptions = vertex_reflection.get_vertex_attribute_descriptions();
    const spirv::reflection_type fragment_reflection(fragment_spirv_code);
    const DescriptorSetLayoutBindingsType fragment_descriptor_set_layout_bindings = fragment_reflection.get_descriptor_set_layout_bindings();
    const DescriptorSetLayoutBindingsType merged_descriptor_set_layout_bindings = merge_descriptor_set_layout_bingins(vertex_descriptor_set_layout_bindings,
        fragment_descriptor_set_layout_bindings);

    // Create descriptor set layouts
    std::vector<vk::raii::DescriptorSetLayout> descriptor_set_layouts;
    for(const auto& descriptor_set_layout_binding: merged_descriptor_set_layout_bindings)
    {
        const vk::DescriptorSetLayoutCreateInfo descriptor_set_layout_create_info(vk::DescriptorSetLayoutCreateFlags{},
            static_cast<uint32_t>(descriptor_set_layout_binding.size()),
            descriptor_set_layout_binding.data());
        descriptor_set_layouts.emplace_back(_device.createDescriptorSetLayout(descriptor_set_layout_create_info));
    }

    // Create pipeline layout
    std::vector<vk::DescriptorSetLayout> descriptor_set_layouts_vector;
    std::transform(descriptor_set_layouts.begin(),
        descriptor_set_layouts.end(),
        std::back_inserter(descriptor_set_layouts_vector),
        [](const vk::raii::DescriptorSetLayout& descriptor_set_layout)
            {
                return *descriptor_set_layout;
            });
    const vk::PipelineLayoutCreateInfo pipeline_layout_create_info(vk::PipelineLayoutCreateFlags{},
        static_cast<uint32_t>(descriptor_set_layouts_vector.size()),
        descriptor_set_layouts_vector.data());
    vk::raii::PipelineLayout pipeline_layout = _device.createPipelineLayout(pipeline_layout_create_info);

    // Create shader
    _shaders.emplace_back(std::move(vertex_shader),
        std::move(fragment_shader),
        std::move(merged_descriptor_set_layout_bindings),
        std::move(descriptor_set_layouts),
        std::move(vertex_input_attribute_descriptions),
        std::move(pipeline_layout));

    return shader_handle_type(_shaders.size() - 1);
}

yoe::renderer_type::material_handle_type yoe::renderer_type::create_material(shader_handle_type shader,
    image_handle_type albedo)
{
    _materials.emplace_back(shader, albedo);

    return material_handle_type(_materials.size() - 1);
}

void yoe::renderer_type::check_required_instance_extensions_support(const std::vector<const char*>& required_extension_names) const
{
    const std::vector<vk::ExtensionProperties> instance_extensions = _context.enumerateInstanceExtensionProperties();

    if(const auto [success, missing_extension] = check_extensions(required_extension_names, instance_extensions); !success)
    {
        throw exception_type("Required extension '{}' is not supported by the instance!", missing_extension);
    }
}

void yoe::renderer_type::check_required_instance_layers_support(const std::vector<const char*>& required_layer_names) const
{
    const std::vector<vk::LayerProperties> instance_layers = _context.enumerateInstanceLayerProperties();

    if(const auto [success, missing_layer] = check_layers(required_layer_names, instance_layers); !success)
    {
        throw exception_type("Required layer '{}' is not supported by the instance!", missing_layer);
    }
}

yoe::renderer_type::PhysicalDevice yoe::renderer_type::select_physical_device(const std::vector<const char*>& required_extension_names,
    const std::vector<const char*>& required_layer_names) const
{
    const std::vector<vk::raii::PhysicalDevice> physical_devices = _instance.enumeratePhysicalDevices();
    std::optional<PhysicalDevice> result;

    for(const auto& physical_device: physical_devices)
    {
        const std::vector<vk::ExtensionProperties> physical_device_extensions = physical_device.enumerateDeviceExtensionProperties();
        if(!check_extensions(required_extension_names, physical_device_extensions).success)
        {
            continue;
        }

        const std::vector<vk::LayerProperties> physical_device_layers = physical_device.enumerateDeviceLayerProperties();
        if(!check_layers(required_layer_names, physical_device_layers).success)
        {
            continue;
        }

        std::vector<vk::QueueFamilyProperties> queue_family_properties = physical_device.getQueueFamilyProperties();
        std::optional<uint32_t> graphics_queue_family_index;
        std::optional<uint32_t> compute_queue_family_index;
        std::optional<uint32_t> transfer_queue_family_index;
        std::optional<uint32_t> presentation_queue_family_index;

        enum class queue_type: uint8_t
        {
            graphics,
            compute,
            transfer,
            presentation
        };

        std::map<queue_type, std::vector<uint32_t>> map;
        auto has_all = [&map]()
        {
            return map.find(queue_type::graphics) != map.end() &&
                map.find(queue_type::compute) != map.end() &&
                map.find(queue_type::transfer) != map.end() &&
                map.find(queue_type::presentation) != map.end();
        };

        // Go through all the queue families and group them
        for(uint32_t queue_family_index = 0; queue_family_index < queue_family_properties.size(); ++queue_family_index)
        {
            const auto& queue_family_property = queue_family_properties[queue_family_index];
            if(queue_family_property.queueCount == 0)
            {
                continue;
            }
            if(queue_family_property.queueFlags & vk::QueueFlagBits::eGraphics)
            {
                map[queue_type::graphics].push_back(queue_family_index);
            }
            if(queue_family_property.queueFlags & vk::QueueFlagBits::eCompute)
            {
                map[queue_type::compute].push_back(queue_family_index);
            }
            if(queue_family_property.queueFlags & vk::QueueFlagBits::eTransfer)
            {
                map[queue_type::transfer].push_back(queue_family_index);
            }
            if(physical_device.getSurfaceSupportKHR(queue_family_index, *_surface) == VK_TRUE)
            {
                map[queue_type::presentation].push_back(queue_family_index);
            }
        }
        // If not all the queues are supported continue searching
        if(!has_all())
        {
            continue;
        }
        // Get the wanted queues. We want the most "diverse" queue set, so different queues for different roles - if possible
        auto get_queue = [&map](queue_type type, std::set<uint32_t>& used_queues)
        {
            const auto& queues = map[type];
            for(auto queue: queues)
            {
                if(!used_queues.contains(queue))
                {
                    used_queues.insert(queue);
                    return queue;
                }
            }

            return queues.front();
        };
        std::set<uint32_t> used_queues;
        graphics_queue_family_index = get_queue(queue_type::graphics, used_queues);
        compute_queue_family_index = get_queue(queue_type::compute, used_queues);
        transfer_queue_family_index = get_queue(queue_type::transfer, used_queues);
        presentation_queue_family_index = get_queue(queue_type::presentation, used_queues);

        // If this is a descrete GPU, use this one
        const vk::PhysicalDeviceProperties physical_device_properties = physical_device.getProperties();
        if(physical_device_properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu)
        {
            result = PhysicalDevice{
                .physical_device = physical_device,
                .graphics_queue_family_index = graphics_queue_family_index.value(),
                .compute_queue_family_index = compute_queue_family_index.value(),
                .transfer_queue_family_index = transfer_queue_family_index.value(),
                .presentation_queue_family_index = presentation_queue_family_index.value(),
            };
            break;
        }

        // This physical device is a good candidate, but continue searching in the hope for a better candidate
        result = PhysicalDevice{
            .physical_device = physical_device,
            .graphics_queue_family_index = graphics_queue_family_index.value(),
            .compute_queue_family_index = compute_queue_family_index.value(),
            .transfer_queue_family_index = transfer_queue_family_index.value(),
            .presentation_queue_family_index = presentation_queue_family_index.value(),
        };
    }

    if(!result.has_value())
    {
        throw exception_type("No suitable physical device could be found!");
    }

    return result.value();
}

vk::raii::Device yoe::renderer_type::create_device(const std::vector<const char*>& required_extensions,
    const std::vector<const char*>& required_layers) const
{
    const std::set<uint32_t> queue_indices = {
        _physical_device.graphics_queue_family_index,
        _physical_device.compute_queue_family_index,
        _physical_device.transfer_queue_family_index,
        _physical_device.presentation_queue_family_index
    };
    std::vector<vk::DeviceQueueCreateInfo> device_queue_create_infos;
    constexpr float priority = 1.0f;

    device_queue_create_infos.reserve(queue_indices.size());
    for(const uint32_t queue_index: queue_indices)
    {
        device_queue_create_infos.emplace_back(vk::DeviceQueueCreateFlags(),
            queue_index,
            1, // queue count
            &priority);
    }

    // Enable multi draw
    const auto chain = _physical_device.physical_device.getFeatures2<vk::PhysicalDeviceFeatures2, vk::PhysicalDeviceMultiDrawFeaturesEXT>();
    vk::PhysicalDeviceFeatures2 physical_device_features2 = chain.get<>();
    vk::PhysicalDeviceMultiDrawFeaturesEXT multi_draw_features_ext = chain.get<vk::PhysicalDeviceMultiDrawFeaturesEXT>();

    if(multi_draw_features_ext.multiDraw == VK_FALSE)
    {
        throw exception_type("Physical device does not support multi draw!");
    }

    constexpr vk::PhysicalDeviceFeatures physical_device_features;
    const vk::DeviceCreateInfo device_create_info(vk::DeviceCreateFlags(),
        static_cast<uint32_t>(device_queue_create_infos.size()),
        device_queue_create_infos.data(),
        static_cast<uint32_t>(required_layers.size()),
        required_layers.data(),
        static_cast<uint32_t>(required_extensions.size()),
        required_extensions.data(),
        nullptr,
        &physical_device_features2);

    return _physical_device.physical_device.createDevice(device_create_info);
}

yoe::renderer_type::SwapchainT yoe::renderer_type::create_swapchain() const
{
    const vk::SurfaceCapabilitiesKHR surface_capabilities = _physical_device.physical_device.getSurfaceCapabilitiesKHR(*_surface);
    const std::vector<vk::SurfaceFormatKHR> surface_formats = _physical_device.physical_device.getSurfaceFormatsKHR(*_surface);
    const std::vector<vk::PresentModeKHR> present_modes = _physical_device.physical_device.getSurfacePresentModesKHR(*_surface);

    // Select number of images
    const uint32_t image_count = [&surface_capabilities]()
    {
        uint32_t result = surface_capabilities.minImageCount + 1;

        if(surface_capabilities.maxImageCount > 0 && result > surface_capabilities.maxImageCount)
        {
            result = surface_capabilities.maxImageCount;
        }

        return result;
    }();

    // Select surface format
    const vk::SurfaceFormatKHR surface_format = [&surface_formats]()
    {
        // If there is only one surface format and it is UNDEFINED, we can choose whatever we want
        if(surface_formats.size() == 1 && surface_formats[0].format == vk::Format::eUndefined)
        {
            return vk::SurfaceFormatKHR(vk::Format::eR8G8B8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear);
        }

        // Check if there is a supported surface format we want (RGBA8_UNORM, SRGB)
        for(const vk::SurfaceFormatKHR& surface_format : surface_formats)
        {
            if(surface_format.format == vk::Format::eR8G8B8A8Unorm && surface_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            {
                return surface_format;
            }
        }

        // If not just use what we have
        return surface_formats[0];
    }();

    // Select image dimensions
    const vk::Extent2D extent = [&surface_capabilities]()
    {
        // If width and heigth are -1, we can choose freely between the min and max extents
        if(surface_capabilities.currentExtent.width == -1)
        {
            // TODO: Image dimansions are hardcoded
            const uint32_t width = std::clamp(1920u, surface_capabilities.minImageExtent.width, surface_capabilities.maxImageExtent.width);
            const uint32_t height = std::clamp(1080u, surface_capabilities.minImageExtent.height, surface_capabilities.maxImageExtent.height);

            return vk::Extent2D(width, height);
        }

        // Otherwise just return the current extent
        return surface_capabilities.currentExtent;
    }();

    // Select usages - we only want color attachments
    // TODO: Do we want depth?
    constexpr vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eColorAttachment;

    // Select surface transform - we use identity if not forced to use something else
    const vk::SurfaceTransformFlagsKHR surface_transform = [&surface_capabilities]()
    {
        if(surface_capabilities.currentTransform & vk::SurfaceTransformFlagBitsKHR::eIdentity)
        {
            return vk::SurfaceTransformFlagBitsKHR::eIdentity;
        }

        return surface_capabilities.currentTransform;
    }();

    // Select present mode
    const vk::PresentModeKHR present_mode = [&present_modes]()
    {
        for(const vk::PresentModeKHR present_mode: present_modes)
        {
            if(present_mode == vk::PresentModeKHR::eMailbox)
            {
                return present_mode;
            }
        }

        // FIFO should always be available
        return vk::PresentModeKHR::eFifo;
    }();

    // Finally create the swapchain
    const vk::SwapchainCreateInfoKHR swapchain_create_info(vk::SwapchainCreateFlagsKHR(),
        *_surface,
        image_count,
        surface_format.format,
        surface_format.colorSpace,
        extent,
        1, // array layers
        usage,
        vk::SharingMode::eExclusive,
        0, // queue family index count
        nullptr, // queue family indeices
        // TODO: This is super ugly! Maybe write an issue on GitHub?
        static_cast<vk::SurfaceTransformFlagBitsKHR>(static_cast<vk::SurfaceTransformFlagsKHR::MaskType>(surface_transform)),
        vk::CompositeAlphaFlagBitsKHR::eOpaque,
        present_mode,
        {}, // clipped
        {} /*old swapchain*/);

    vk::raii::SwapchainKHR swapchain = _device.createSwapchainKHR(swapchain_create_info);

    // Add labels to the swapchain images
    uint32_t counter = 0;
    for(const auto& image : swapchain.getImages())
    {
        const std::string label = "Swapchain Image " + std::to_string(counter++);
        label_object(image, label.c_str(), _device);
    }

    return { std::move(swapchain), swapchain_create_info };
}

vk::raii::ShaderModule yoe::renderer_type::create_shader_module(compiler_type::shader_kind kind,
    const std::vector<uint32_t>& spirv_code) const
{
    const vk::ShaderModuleCreateInfo shader_module_create_info(vk::ShaderModuleCreateFlags(),
        spirv_code.size() * sizeof(uint32_t),
        spirv_code.data());
    return _device.createShaderModule(shader_module_create_info);
}

yoe::memory_allocator_type::buffer_type
yoe::renderer_type::create_buffer(VkDeviceSize size, VkBufferUsageFlags usage, bool map, const void* data) const
{
    VkBufferCreateInfo const buffer_create_info = {
        .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .size = size,
        .usage = usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
        .queueFamilyIndexCount = 1, // The last two element is ignored since sharing mdoe is not concurrent
        .pQueueFamilyIndices = &_physical_device.graphics_queue_family_index
    };

    memory_allocator_type::buffer_type buffer = _memory_allocator.create_buffer(buffer_create_info, map);

    if(data != nullptr)
    {
        VkBufferCreateInfo const staging_buffer_create_info = {
            .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
            .pNext = nullptr,
            .flags = 0,
            .size = size,
            .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
            .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .queueFamilyIndexCount = 1, // The last two element is ignored since sharing mdoe is not concurrent
            .pQueueFamilyIndices = &_physical_device.transfer_queue_family_index,
        };
        const memory_allocator_type::buffer_type staging_buffer = _memory_allocator.create_buffer(staging_buffer_create_info, true);
        {
            auto mapped_memory = staging_buffer.map_memory();
            memcpy(mapped_memory.get_pointer(), data, size);
        }


        vk::CommandBufferBeginInfo const command_buffer_begin_info(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
        _transfer_command_buffer.begin(command_buffer_begin_info);

        vk::BufferCopy const buffer_copy(0, 0, size);
        _transfer_command_buffer.copyBuffer(staging_buffer.get_handle(), buffer.get_handle(), { buffer_copy });

        _transfer_command_buffer.end();

        vk::SubmitInfo const submit_info(0, nullptr, nullptr, 1, &*_transfer_command_buffer);
        _transfer_queue.submit(submit_info);
        _transfer_queue.waitIdle();
    }

    return buffer;
}

yoe::renderer_type::image_handle_type yoe::renderer_type::create_image(const char* path)
{
    const image_type loaded_image(path);

    return create_image(loaded_image);
}

size_t yoe::renderer_type::get_pipeline_hash(mesh_handle_type mesh_handle) const
{
    const mesh_data_type& mesh_data = _meshes[mesh_handle._value];
    const material_data_type& material_data = _materials[mesh_data._material._value];
    const shader_data_type& shader_data = _shaders[material_data._shader._value];

    size_t seed = 0;
    hash_combine(seed, mesh_data._stride_size);

    const VkShaderModule vertex_shader = *shader_data._vertex_shader;
    hash_combine(seed, vertex_shader);

    const VkShaderModule fragment_shader = *shader_data._fragment_shader;
    hash_combine(seed, fragment_shader);

    VkPipelineLayout pipeline_layout = *shader_data._pipeline_layout;
    hash_combine(seed, pipeline_layout);

    return seed;
}
