#include <yoe/window.hh>
#include <SDL.h>
#include <SDL_syswm.h>

namespace
{
    SDL_SysWMinfo get_window_info(SDL_Window* window)
    {
        SDL_SysWMinfo info;
        SDL_VERSION(&info.version);
        if(!SDL_GetWindowWMInfo(window, &info))
        {
            throw yoe::window_type::exception_type("Failed to get window info!");
        }

        return info;
    }
}

yoe::window_type::initialiser_type::initialiser_type()
{
    window_type::initialise();
}

yoe::window_type::initialiser_type::~initialiser_type()
{
    window_type::deinitialize();
}

yoe::window_type::window_type()
{
    // TODO: Use arguments for the parameters here
    // We can toggle full screen, see SDL_SetWindowFullscreen
    // TODO: Using SDL_WINDOW_FULLSCREEN crashes the renderer because the surface is out of date
    _window = SDL_CreateWindow("Yoe-C App", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1920, 1080, SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP);
    if(nullptr == _window)
    {
        throw exception_type("Failed to create window!");
    }
    // SDL_ShowCursor(SDL_DISABLE);
}

bool yoe::window_type::run()
{
    SDL_Event event;

    while(SDL_PollEvent(&event))
    {
        switch (event.type)
        {
            case SDL_QUIT:
                return false;
            case SDL_WINDOWEVENT:
                switch (event.window.event)
                {
                    case SDL_WINDOWEVENT_CLOSE:
                        return false;
                    default:
                        break;
                }
                break;
            case SDL_KEYUP:
                switch(event.key.keysym.sym)
                {
                    case SDLK_ESCAPE:
                        return false;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }

    return true;
}

HINSTANCE yoe::window_type::get_hinstance() const
{
    return get_window_info(_window).info.win.hinstance;
}

HWND yoe::window_type::get_hwnd() const
{
    return get_window_info(_window).info.win.window;
}

void yoe::window_type::initialise()
{
    if(SDL_Init(SDL_INIT_VIDEO) < 0)
    {
        throw exception_type("Failed to initialise SDL");
    }
}

void yoe::window_type::deinitialize()
{
    SDL_Quit();
}
