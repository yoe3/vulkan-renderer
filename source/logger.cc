#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/sinks/basic_file_sink.h>
#include <yoe/logger.hh>

namespace
{
    spdlog::level::level_enum convert(yoe::logging::log_level_type log_level)
    {
        switch(log_level)
        {
            case yoe::logging::log_level_type::debug:
                return spdlog::level::level_enum::debug;
            case yoe::logging::log_level_type::info:
                return spdlog::level::level_enum::info;
            case yoe::logging::log_level_type::warning:
                return spdlog::level::level_enum::warn;
            case yoe::logging::log_level_type::error:
                return spdlog::level::level_enum::err;
            case yoe::logging::log_level_type::critical:
                return spdlog::level::level_enum::critical;
        }

        return spdlog::level::level_enum::err;
    }

    std::shared_ptr<spdlog::sinks::basic_file_sink_mt> log_file_sink;
} // namespace

bool yoe::logging::initialize(log_level_type log_level,
    const char* path,
    const char* format)
{
    spdlog::level::level_enum level = convert(log_level);
    // TODO: Create only multi threaded sinks if requested
    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    if(!console_sink)
    {
        return false;
    }
    console_sink->set_level(level);

    const char* log_path = path == nullptr ? "yoe.log" : path;
    auto file_sink = std::make_shared<spdlog::sinks::basic_file_sink_mt>(log_path, false);
    if(!file_sink)
    {
        return false;
    }
    file_sink->set_level(level);

    log_file_sink = file_sink;

    spdlog::sinks_init_list sinks = { console_sink, file_sink };
    auto logger = std::make_shared<spdlog::logger>("default", sinks);
    if(!logger)
    {
        return false;
    }
    logger->set_level(level);
    spdlog::set_default_logger(logger);
    spdlog::set_level(level);
    if(format != nullptr)
    {
        spdlog::set_pattern(format);
    }

    return true;
}

// See:
// https://github.com/gabime/spdlog/wiki/1.-QuickStart#create-multiple-loggers-that-share-the-same-file-sink-aka-categories
yoe::logging::logger_type::logger_type(const char* name, log_level_type log_level, const char* format)
{
    spdlog::level::level_enum level = convert(log_level);
    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    console_sink->set_level(level);

    spdlog::sinks_init_list sinks = { console_sink, log_file_sink };
    _logger = std::make_shared<spdlog::logger>(name, sinks);
    if(!_logger)
    {
        throw exception_type("Failed to create logger with name {}", name);
    }
    _logger->set_level(level);
    if(format != nullptr)
    {
        _logger->set_pattern(format);
    }
}
