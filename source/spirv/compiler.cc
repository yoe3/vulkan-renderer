#include <yoe/spirv/compiler.hh>
#include <yoe/core/enum.hh>
#include <yoe/spirv/reflection.hh>

//#include <glslang/>
#include <glslang/Public/ShaderLang.h>
// #include <glslang/Public/ResourceLimits.h>
#include <glslang/SPIRV/GlslangToSpv.h>

namespace
{
    EShLanguage convert(yoe::compiler_type::shader_kind kind)
    {
        switch (kind)
        {
            case yoe::compiler_type::shader_kind::VERTEX:
                return EShLangVertex;
            case yoe::compiler_type::shader_kind::FRAGMENT:
                return EShLangFragment;
            default:
                throw yoe::compiler_type::exception_type("Unknown shader kind: {}", yoe::enum_to_underlying_type(kind));
        }
    }

    // I wish this was not needed. Looks like a new method is cmoing for this:
    // https://github.com/KhronosGroup/glslang/blob/main/glslang/Public/ResourceLimits.h
    //
    const TBuiltInResource DefaultTBuiltInResource = {
    /* .MaxLights = */ 32,
    /* .MaxClipPlanes = */ 6,
    /* .MaxTextureUnits = */ 32,
    /* .MaxTextureCoords = */ 32,
    /* .MaxVertexAttribs = */ 64,
    /* .MaxVertexUniformComponents = */ 4096,
    /* .MaxVaryingFloats = */ 64,
    /* .MaxVertexTextureImageUnits = */ 32,
    /* .MaxCombinedTextureImageUnits = */ 80,
    /* .MaxTextureImageUnits = */ 32,
    /* .MaxFragmentUniformComponents = */ 4096,
    /* .MaxDrawBuffers = */ 32,
    /* .MaxVertexUniformVectors = */ 128,
    /* .MaxVaryingVectors = */ 8,
    /* .MaxFragmentUniformVectors = */ 16,
    /* .MaxVertexOutputVectors = */ 16,
    /* .MaxFragmentInputVectors = */ 15,
    /* .MinProgramTexelOffset = */ -8,
    /* .MaxProgramTexelOffset = */ 7,
    /* .MaxClipDistances = */ 8,
    /* .MaxComputeWorkGroupCountX = */ 65535,
    /* .MaxComputeWorkGroupCountY = */ 65535,
    /* .MaxComputeWorkGroupCountZ = */ 65535,
    /* .MaxComputeWorkGroupSizeX = */ 1024,
    /* .MaxComputeWorkGroupSizeY = */ 1024,
    /* .MaxComputeWorkGroupSizeZ = */ 64,
    /* .MaxComputeUniformComponents = */ 1024,
    /* .MaxComputeTextureImageUnits = */ 16,
    /* .MaxComputeImageUniforms = */ 8,
    /* .MaxComputeAtomicCounters = */ 8,
    /* .MaxComputeAtomicCounterBuffers = */ 1,
    /* .MaxVaryingComponents = */ 60,
    /* .MaxVertexOutputComponents = */ 64,
    /* .MaxGeometryInputComponents = */ 64,
    /* .MaxGeometryOutputComponents = */ 128,
    /* .MaxFragmentInputComponents = */ 128,
    /* .MaxImageUnits = */ 8,
    /* .MaxCombinedImageUnitsAndFragmentOutputs = */ 8,
    /* .MaxCombinedShaderOutputResources = */ 8,
    /* .MaxImageSamples = */ 0,
    /* .MaxVertexImageUniforms = */ 0,
    /* .MaxTessControlImageUniforms = */ 0,
    /* .MaxTessEvaluationImageUniforms = */ 0,
    /* .MaxGeometryImageUniforms = */ 0,
    /* .MaxFragmentImageUniforms = */ 8,
    /* .MaxCombinedImageUniforms = */ 8,
    /* .MaxGeometryTextureImageUnits = */ 16,
    /* .MaxGeometryOutputVertices = */ 256,
    /* .MaxGeometryTotalOutputComponents = */ 1024,
    /* .MaxGeometryUniformComponents = */ 1024,
    /* .MaxGeometryVaryingComponents = */ 64,
    /* .MaxTessControlInputComponents = */ 128,
    /* .MaxTessControlOutputComponents = */ 128,
    /* .MaxTessControlTextureImageUnits = */ 16,
    /* .MaxTessControlUniformComponents = */ 1024,
    /* .MaxTessControlTotalOutputComponents = */ 4096,
    /* .MaxTessEvaluationInputComponents = */ 128,
    /* .MaxTessEvaluationOutputComponents = */ 128,
    /* .MaxTessEvaluationTextureImageUnits = */ 16,
    /* .MaxTessEvaluationUniformComponents = */ 1024,
    /* .MaxTessPatchComponents = */ 120,
    /* .MaxPatchVertices = */ 32,
    /* .MaxTessGenLevel = */ 64,
    /* .MaxViewports = */ 16,
    /* .MaxVertexAtomicCounters = */ 0,
    /* .MaxTessControlAtomicCounters = */ 0,
    /* .MaxTessEvaluationAtomicCounters = */ 0,
    /* .MaxGeometryAtomicCounters = */ 0,
    /* .MaxFragmentAtomicCounters = */ 8,
    /* .MaxCombinedAtomicCounters = */ 8,
    /* .MaxAtomicCounterBindings = */ 1,
    /* .MaxVertexAtomicCounterBuffers = */ 0,
    /* .MaxTessControlAtomicCounterBuffers = */ 0,
    /* .MaxTessEvaluationAtomicCounterBuffers = */ 0,
    /* .MaxGeometryAtomicCounterBuffers = */ 0,
    /* .MaxFragmentAtomicCounterBuffers = */ 1,
    /* .MaxCombinedAtomicCounterBuffers = */ 1,
    /* .MaxAtomicCounterBufferSize = */ 16384,
    /* .MaxTransformFeedbackBuffers = */ 4,
    /* .MaxTransformFeedbackInterleavedComponents = */ 64,
    /* .MaxCullDistances = */ 8,
    /* .MaxCombinedClipAndCullDistances = */ 8,
    /* .MaxSamples = */ 4,
    /* .maxMeshOutputVerticesNV = */ 256,
    /* .maxMeshOutputPrimitivesNV = */ 512,
    /* .maxMeshWorkGroupSizeX_NV = */ 32,
    /* .maxMeshWorkGroupSizeY_NV = */ 1,
    /* .maxMeshWorkGroupSizeZ_NV = */ 1,
    /* .maxTaskWorkGroupSizeX_NV = */ 32,
    /* .maxTaskWorkGroupSizeY_NV = */ 1,
    /* .maxTaskWorkGroupSizeZ_NV = */ 1,
    /* .maxMeshViewCountNV = */ 4,
    /* .maxDualSourceDrawBuffersEXT = */ 1,

    /* .limits = */ {
        /* .nonInductiveForLoops = */ 1,
        /* .whileLoops = */ 1,
        /* .doWhileLoops = */ 1,
        /* .generalUniformIndexing = */ 1,
        /* .generalAttributeMatrixVectorIndexing = */ 1,
        /* .generalVaryingIndexing = */ 1,
        /* .generalSamplerIndexing = */ 1,
        /* .generalVariableIndexing = */ 1,
        /* .generalConstantMatrixVectorIndexing = */ 1,
    }
};
}

template<>
const char* yoe::enum_to_string<yoe::compiler_type::shader_kind>(compiler_type::shader_kind value)
{
    switch(value)
    {
        case compiler_type::shader_kind::VERTEX:
            return "VERTEX";
        case compiler_type::shader_kind::FRAGMENT:
            return "FRAGMENT";
        default:
            throw compiler_type::exception_type("Unknown shader kind: {}", yoe::enum_to_underlying_type(value));
    }
}

yoe::compiler_type::initialiser_type::initialiser_type()
{
    glslang::InitializeProcess();
}

yoe::compiler_type::initialiser_type::~initialiser_type()
{
    glslang::FinalizeProcess();
}

std::vector<uint32_t> yoe::compiler_type::compile(shader_kind kind, const char* const* code, size_t count)
{
    const EShLanguage language = convert(kind);
    glslang::TShader shader(language);

    shader.setStrings(code, static_cast<int>(count));
    shader.setEnvInput(glslang::EShSourceGlsl, language, glslang::EShClientVulkan, 100);
    shader.setEnvClient(glslang::EShClientVulkan, glslang::EShTargetVulkan_1_2);
    shader.setEnvTarget(glslang::EShTargetSpv, glslang::EShTargetSpv_1_0);
    if(!shader.parse(&DefaultTBuiltInResource, 100, EProfile::ECoreProfile, false, false, EShMessages::EShMsgDefault))
    {
        throw exception_type("Failed to parse {} shader: {}", enum_to_string(kind), shader.getInfoLog());
    }

    glslang::TProgram program;

    program.addShader(&shader);
    if(!program.link(EShMsgDefault))
    {
        throw exception_type("Failed to link {} shader: {}", enum_to_string(kind), program.getInfoLog());
    }

    const glslang::TIntermediate* intermediate = program.getIntermediate(language);

    if(nullptr == intermediate)
    {
        throw exception_type("Could not get intermediate code for {} program!", enum_to_string(kind));
    }

    std::vector<uint32_t> spirv_code;
    glslang::SpvOptions options;
#ifdef YOE_DEBUG
    options.generateDebugInfo = true;
    options.stripDebugInfo = false;
    options.disableOptimizer = true;
#else
    options.generateDebugInfo = false;
    options.stripDebugInfo = true;
    options.disableOptimizer = false;
#endif
    options.optimizeSize = true;
    options.disassemble = false;
    options.validate = true;

    glslang::GlslangToSpv(*intermediate, spirv_code, &options);

    return spirv_code;
}
