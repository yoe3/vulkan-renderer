#include <yoe/spirv/reflection.hh>
#include "spirv_reflect.h"

yoe::spirv::reflection_type::reflection_type(const std::vector<uint32_t>& code):
    _shader_module(code)
{
    if(_shader_module.GetResult() != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to parse SPIRV code!");
    }
}

namespace
{
    vk::DescriptorType convert(SpvReflectDescriptorType type)
    {
        // TODO: Looks like the SpvReflect enums have the same values as VK enums
        switch(type)
        {
            case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLER:
                return vk::DescriptorType::eSampler;
            case SPV_REFLECT_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER:
                return vk::DescriptorType::eCombinedImageSampler;
            case SPV_REFLECT_DESCRIPTOR_TYPE_SAMPLED_IMAGE:
                return vk::DescriptorType::eSampledImage;
            case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_IMAGE:
                return vk::DescriptorType::eStorageImage;
            case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER:
                return vk::DescriptorType::eUniformTexelBuffer;
            case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER:
                return vk::DescriptorType::eStorageTexelBuffer;
            case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER:
                return vk::DescriptorType::eUniformBuffer;
            case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER:
                return vk::DescriptorType::eStorageBuffer;
            case SPV_REFLECT_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC:
                return vk::DescriptorType::eUniformBufferDynamic;
            case SPV_REFLECT_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC:
                return vk::DescriptorType::eStorageBufferDynamic;
            case SPV_REFLECT_DESCRIPTOR_TYPE_INPUT_ATTACHMENT:
                return vk::DescriptorType::eInputAttachment;
            case SPV_REFLECT_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR:
                return vk::DescriptorType::eAccelerationStructureKHR;
        }

        throw yoe::spirv::reflection_type::exception_type("Unknown descriptor type received: {}", static_cast<int32_t>(type));
    }

    vk::ShaderStageFlags convert(SpvReflectShaderStageFlagBits flags)
    {
        vk::ShaderStageFlags result = {};

        if(flags & SPV_REFLECT_SHADER_STAGE_VERTEX_BIT)
        {
            result |= vk::ShaderStageFlagBits::eVertex;
        }
        if(flags & SPV_REFLECT_SHADER_STAGE_TESSELLATION_CONTROL_BIT)
        {
            result |= vk::ShaderStageFlagBits::eTessellationControl;
        }
        if(flags & SPV_REFLECT_SHADER_STAGE_TESSELLATION_EVALUATION_BIT)
        {
            result |= vk::ShaderStageFlagBits::eTessellationEvaluation;
        }
        if(flags & SPV_REFLECT_SHADER_STAGE_GEOMETRY_BIT)
        {
            result |= vk::ShaderStageFlagBits::eGeometry;
        }
        if(flags & SPV_REFLECT_SHADER_STAGE_FRAGMENT_BIT)
        {
            result |= vk::ShaderStageFlagBits::eFragment;
        }
        if(flags & SPV_REFLECT_SHADER_STAGE_COMPUTE_BIT)
        {
            result |= vk::ShaderStageFlagBits::eCompute;
        }

        // TODO: Implement the rest of it

        return result;
        // typedef enum SpvReflectShaderStageFlagBits {
        //     SPV_REFLECT_SHADER_STAGE_TASK_BIT_NV                 = 0x00000040, // = VK_SHADER_STAGE_TASK_BIT_NV
        //     SPV_REFLECT_SHADER_STAGE_TASK_BIT_EXT                = SPV_REFLECT_SHADER_STAGE_TASK_BIT_NV, // = VK_SHADER_STAGE_CALLABLE_BIT_EXT
        //     SPV_REFLECT_SHADER_STAGE_MESH_BIT_NV                 = 0x00000080, // = VK_SHADER_STAGE_MESH_BIT_NV
        //     SPV_REFLECT_SHADER_STAGE_MESH_BIT_EXT                = SPV_REFLECT_SHADER_STAGE_MESH_BIT_NV, // = VK_SHADER_STAGE_CALLABLE_BIT_EXT
        //     SPV_REFLECT_SHADER_STAGE_RAYGEN_BIT_KHR              = 0x00000100, // = VK_SHADER_STAGE_RAYGEN_BIT_KHR
        //     SPV_REFLECT_SHADER_STAGE_ANY_HIT_BIT_KHR             = 0x00000200, // = VK_SHADER_STAGE_ANY_HIT_BIT_KHR
        //     SPV_REFLECT_SHADER_STAGE_CLOSEST_HIT_BIT_KHR         = 0x00000400, // = VK_SHADER_STAGE_CLOSEST_HIT_BIT_KHR
        //     SPV_REFLECT_SHADER_STAGE_MISS_BIT_KHR                = 0x00000800, // = VK_SHADER_STAGE_MISS_BIT_KHR
        //     SPV_REFLECT_SHADER_STAGE_INTERSECTION_BIT_KHR        = 0x00001000, // = VK_SHADER_STAGE_INTERSECTION_BIT_KHR
        //     SPV_REFLECT_SHADER_STAGE_CALLABLE_BIT_KHR            = 0x00002000, // = VK_SHADER_STAGE_CALLABLE_BIT_KHR
        //
        //   }
    }

    vk::Format convert(SpvReflectFormat format)
    {
        return static_cast<vk::Format>(format);
  //       SPV_REFLECT_FORMAT_UNDEFINED           =   0, // = VK_FORMAT_UNDEFINED
  // SPV_REFLECT_FORMAT_R16_UINT            =  74, // = VK_FORMAT_R16_UINT
  // SPV_REFLECT_FORMAT_R16_SINT            =  75, // = VK_FORMAT_R16_SINT
  // SPV_REFLECT_FORMAT_R16_SFLOAT          =  76, // = VK_FORMAT_R16_SFLOAT
  // SPV_REFLECT_FORMAT_R16G16_UINT         =  81, // = VK_FORMAT_R16G16_UINT
  // SPV_REFLECT_FORMAT_R16G16_SINT         =  82, // = VK_FORMAT_R16G16_SINT
  // SPV_REFLECT_FORMAT_R16G16_SFLOAT       =  83, // = VK_FORMAT_R16G16_SFLOAT
  // SPV_REFLECT_FORMAT_R16G16B16_UINT      =  88, // = VK_FORMAT_R16G16B16_UINT
  // SPV_REFLECT_FORMAT_R16G16B16_SINT      =  89, // = VK_FORMAT_R16G16B16_SINT
  // SPV_REFLECT_FORMAT_R16G16B16_SFLOAT    =  90, // = VK_FORMAT_R16G16B16_SFLOAT
  // SPV_REFLECT_FORMAT_R16G16B16A16_UINT   =  95, // = VK_FORMAT_R16G16B16A16_UINT
  // SPV_REFLECT_FORMAT_R16G16B16A16_SINT   =  96, // = VK_FORMAT_R16G16B16A16_SINT
  // SPV_REFLECT_FORMAT_R16G16B16A16_SFLOAT =  97, // = VK_FORMAT_R16G16B16A16_SFLOAT
  // SPV_REFLECT_FORMAT_R32_UINT            =  98, // = VK_FORMAT_R32_UINT
  // SPV_REFLECT_FORMAT_R32_SINT            =  99, // = VK_FORMAT_R32_SINT
  // SPV_REFLECT_FORMAT_R32_SFLOAT          = 100, // = VK_FORMAT_R32_SFLOAT
  // SPV_REFLECT_FORMAT_R32G32_UINT         = 101, // = VK_FORMAT_R32G32_UINT
  // SPV_REFLECT_FORMAT_R32G32_SINT         = 102, // = VK_FORMAT_R32G32_SINT
  // SPV_REFLECT_FORMAT_R32G32_SFLOAT       = 103, // = VK_FORMAT_R32G32_SFLOAT
  // SPV_REFLECT_FORMAT_R32G32B32_UINT      = 104, // = VK_FORMAT_R32G32B32_UINT
  // SPV_REFLECT_FORMAT_R32G32B32_SINT      = 105, // = VK_FORMAT_R32G32B32_SINT
  // SPV_REFLECT_FORMAT_R32G32B32_SFLOAT    = 106, // = VK_FORMAT_R32G32B32_SFLOAT
  // SPV_REFLECT_FORMAT_R32G32B32A32_UINT   = 107, // = VK_FORMAT_R32G32B32A32_UINT
  // SPV_REFLECT_FORMAT_R32G32B32A32_SINT   = 108, // = VK_FORMAT_R32G32B32A32_SINT
  // SPV_REFLECT_FORMAT_R32G32B32A32_SFLOAT = 109, // = VK_FORMAT_R32G32B32A32_SFLOAT
  // SPV_REFLECT_FORMAT_R64_UINT            = 110, // = VK_FORMAT_R64_UINT
  // SPV_REFLECT_FORMAT_R64_SINT            = 111, // = VK_FORMAT_R64_SINT
  // SPV_REFLECT_FORMAT_R64_SFLOAT          = 112, // = VK_FORMAT_R64_SFLOAT
  // SPV_REFLECT_FORMAT_R64G64_UINT         = 113, // = VK_FORMAT_R64G64_UINT
  // SPV_REFLECT_FORMAT_R64G64_SINT         = 114, // = VK_FORMAT_R64G64_SINT
  // SPV_REFLECT_FORMAT_R64G64_SFLOAT       = 115, // = VK_FORMAT_R64G64_SFLOAT
  // SPV_REFLECT_FORMAT_R64G64B64_UINT      = 116, // = VK_FORMAT_R64G64B64_UINT
  // SPV_REFLECT_FORMAT_R64G64B64_SINT      = 117, // = VK_FORMAT_R64G64B64_SINT
  // SPV_REFLECT_FORMAT_R64G64B64_SFLOAT    = 118, // = VK_FORMAT_R64G64B64_SFLOAT
  // SPV_REFLECT_FORMAT_R64G64B64A64_UINT   = 119, // = VK_FORMAT_R64G64B64A64_UINT
  // SPV_REFLECT_FORMAT_R64G64B64A64_SINT   = 120, // = VK_FORMAT_R64G64B64A64_SINT
  // SPV_REFLECT_FORMAT_R64G64B64A64_SFLOAT = 121, // = VK_FORMAT_R64G64B64A64_SFLOAT
    }
}

yoe::spirv::reflection_type::DescriptorSetLayoutBindingsType yoe::spirv::reflection_type::get_descriptor_set_layout_bindings() const
{
    std::vector<SpvReflectDescriptorSet*> descriptor_sets = get_descriptor_sets();
    std::vector<std::vector<vk::DescriptorSetLayoutBinding>> result;

    result.resize(descriptor_sets.size());
    for(const SpvReflectDescriptorSet* descriptor_set: descriptor_sets)
    {
        std::vector<vk::DescriptorSetLayoutBinding>& descriptor_set_layout_bindings = result[descriptor_set->set];

        for(uint32_t index = 0; index < descriptor_set->binding_count; ++index)
        {
            SpvReflectDescriptorBinding* binding = descriptor_set->bindings[index];
            vk::DescriptorSetLayoutBinding vk_binding = {};
            vk_binding.binding = binding->binding;
            vk_binding.descriptorType = convert(binding->descriptor_type);
            vk_binding.descriptorCount = binding->count;
            vk_binding.stageFlags = convert(_shader_module.GetShaderStage());

            descriptor_set_layout_bindings.push_back(vk_binding);
        }
    }

    return result;
}

std::vector<vk::VertexInputAttributeDescription> yoe::spirv::reflection_type::get_vertex_attribute_descriptions() const
{
    if(_shader_module.GetShaderStage() != SPV_REFLECT_SHADER_STAGE_VERTEX_BIT)
    {
        throw exception_type("Cannot fetch vertex attributes on non-vertex shader!");
    }
    const std::vector<SpvReflectInterfaceVariable*> input_variables = get_input_variables();
    std::vector<vk::VertexInputAttributeDescription> result;

    result.reserve(input_variables.size());
    for(SpvReflectInterfaceVariable* input_variable: input_variables)
    {
        vk::VertexInputAttributeDescription vertex_input_attribute_description;

        // Binding does not matter here
        vertex_input_attribute_description.binding = 0;
        vertex_input_attribute_description.location = input_variable->location;
        vertex_input_attribute_description.format = convert(input_variable->format);
        // Offset does not matter here either
        vertex_input_attribute_description.offset = 0;

        result.push_back(vertex_input_attribute_description);
    }

    return result;
}

std::vector<SpvReflectInterfaceVariable*> yoe::spirv::reflection_type::get_input_variables() const
{
    uint32_t count = 0;
    std::vector<SpvReflectInterfaceVariable*> result;
    SpvReflectResult spirv_result = _shader_module.EnumerateInputVariables(&count, nullptr);
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }
    result.resize(count);
    spirv_result = _shader_module.EnumerateInputVariables(&count, result.data());
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }

    return result;
}

std::vector<SpvReflectInterfaceVariable*> yoe::spirv::reflection_type::get_output_variables() const
{
    uint32_t count = 0;
    std::vector<SpvReflectInterfaceVariable*> result;
    SpvReflectResult spirv_result = _shader_module.EnumerateOutputVariables(&count, nullptr);
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }
    result.resize(count);
    spirv_result = _shader_module.EnumerateOutputVariables(&count, result.data());
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }

    return result;
}

std::vector<SpvReflectBlockVariable*> yoe::spirv::reflection_type::get_push_constant_blocks() const
{
    uint32_t count = 0;
    std::vector<SpvReflectBlockVariable*> result;
    SpvReflectResult spirv_result = _shader_module.EnumeratePushConstantBlocks(&count, nullptr);
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }
    result.resize(count);
    spirv_result = _shader_module.EnumeratePushConstantBlocks(&count, result.data());
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }

    return result;
}

std::vector<SpvReflectDescriptorBinding*> yoe::spirv::reflection_type::get_descriptor_bindings() const
{
    uint32_t count = 0;
    std::vector<SpvReflectDescriptorBinding*> result;
    SpvReflectResult spirv_result = _shader_module.EnumerateDescriptorBindings(&count, nullptr);
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }
    result.resize(count);
    spirv_result = _shader_module.EnumerateDescriptorBindings(&count, result.data());
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }

    return result;
}

std::vector<SpvReflectDescriptorSet*> yoe::spirv::reflection_type::get_descriptor_sets() const
{
    uint32_t count = 0;
    std::vector<SpvReflectDescriptorSet*> result;
    SpvReflectResult spirv_result = _shader_module.EnumerateDescriptorSets(&count, nullptr);
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }
    result.resize(count);
    spirv_result = _shader_module.EnumerateDescriptorSets(&count, result.data());
    if(spirv_result != SPV_REFLECT_RESULT_SUCCESS)
    {
        throw exception_type("Failed to get input variables!");
    }

    return result;

}
