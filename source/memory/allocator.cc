#include <yoe/memory/allocator.hh>
// TODO: This needs to be done in only one cc file.
//       Maybe place it in a separate cc file
#define VMA_IMPLEMENTATION
#include <vk_mem_alloc.h>

void yoe::memory_allocator_type::initialize(VkInstance instance,
                                            VkPhysicalDevice physical_device,
                                            VkDevice device,
                                            uint32_t vulkanApiVersion)
{
    // Create memory allocator
    const VmaAllocatorCreateInfo allocator_create_info = {
        .physicalDevice = physical_device,
        .device = device,
        .instance = instance,
        .vulkanApiVersion = vulkanApiVersion,
    };
    if(vmaCreateAllocator(&allocator_create_info, &_allocator) != VK_SUCCESS)
    {
        throw exception_type("Failed to create memory allocator!");
    }
}

yoe::memory_allocator_type::buffer_type yoe::memory_allocator_type::create_buffer(const VkBufferCreateInfo& buffer_create_info,
    bool mapped) const
{
    const VmaAllocationCreateInfo allocation_create_info = {
        .flags = mapped ? VMA_ALLOCATION_CREATE_HOST_ACCESS_SEQUENTIAL_WRITE_BIT : static_cast<VmaAllocationCreateFlags>(0),
        .usage = VMA_MEMORY_USAGE_AUTO,
        .requiredFlags = 0,
        .preferredFlags = 0,
        .memoryTypeBits = 0,
        .pool = VK_NULL_HANDLE,
        .pUserData = nullptr,
        .priority = 1.0f
    };
    VkBuffer buffer = VK_NULL_HANDLE;
    VmaAllocation allocation = VK_NULL_HANDLE;

    if(vmaCreateBuffer(_allocator, &buffer_create_info, &allocation_create_info, &buffer, &allocation, nullptr) != VK_SUCCESS)
    {
        throw exception_type("Failed to create buffer!");
    }

    return { _allocator, allocation, buffer };
}

yoe::memory_allocator_type::image_type yoe::memory_allocator_type::create_image(const VkImageCreateInfo& image_create_info) const
{
    constexpr VmaAllocationCreateInfo allocation_create_info = {
        .flags =  0,
        .usage = VMA_MEMORY_USAGE_AUTO,
        .requiredFlags = 0,
        .preferredFlags = 0,
        .memoryTypeBits = 0,
        .pool = VK_NULL_HANDLE,
        .pUserData = nullptr,
        .priority = 1.0f
    };
    VkImage image = VK_NULL_HANDLE;
    VmaAllocation allocation = VK_NULL_HANDLE;

    if(vmaCreateImage(_allocator, &image_create_info, &allocation_create_info, &image, &allocation, nullptr) != VK_SUCCESS)
    {
        throw exception_type("Failed to create image!");
    }

    return { _allocator, allocation, image };
}

yoe::memory_allocator_type::~memory_allocator_type()
{
    if(_allocator != VK_NULL_HANDLE)
    {
        vmaDestroyAllocator(_allocator);
    }
    _allocator = VK_NULL_HANDLE;
}
